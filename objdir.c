#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "nuttx/config.h"
#include <debug.h>

#include "canutils/libcanopen/canopen.h"
#include "canutils/libcanopen/objdir.h"

/* object dictionary management */

/* List of predefined variable types */

static const struct canopen_var_s canopen_types[] = 
{
/* Index                           Sub   Code          Type           Access    Info */
  {DT_BOOLEAN                    , 0x00, OC_DEFTYPE  , DT_UNSIGNED32, 0       ,  1},
  {DT_INTEGER8                   , 0x00, OC_DEFTYPE  , DT_UNSIGNED32, 0       ,  1},
  {DT_INTEGER16                  , 0x00, OC_DEFTYPE  , DT_UNSIGNED32, 0       ,  2},
  {DT_INTEGER32                  , 0x00, OC_DEFTYPE  , DT_UNSIGNED32, 0       ,  4},
  {DT_UNSIGNED8                  , 0x00, OC_DEFTYPE  , DT_UNSIGNED32, 0       ,  1},
  {DT_UNSIGNED16                 , 0x00, OC_DEFTYPE  , DT_UNSIGNED32, 0       ,  2},
  {DT_UNSIGNED32                 , 0x00, OC_DEFTYPE  , DT_UNSIGNED32, 0       ,  4},
  {DT_REAL32                     , 0x00, OC_DEFTYPE  , DT_UNSIGNED32, 0       ,  4},
  {DT_VISSTRING                  , 0x00, OC_DEFTYPE  , DT_UNSIGNED32, 0       ,  sizeof(struct canopen_objdir_dereftype_s)}, /* CIA-301 p. 31 20h-7Eh ASCII chars */
  {DT_OCTSTRING                  , 0x00, OC_DEFTYPE  , DT_UNSIGNED32, 0       ,  sizeof(struct canopen_objdir_dereftype_s)}, /* CIA-301 p. 31 UNSIGNED8 chars */
  {DT_UNISTRING                  , 0x00, OC_DEFTYPE  , DT_UNSIGNED32, 0       ,  sizeof(struct canopen_objdir_dereftype_s)}, /* CIA-301 p. 32 UNSIGNED16 chars */
  {DT_TIMEOFDAY                  , 0x00, OC_DEFTYPE  , DT_UNSIGNED32, 0       ,  6}, /* CIA-301 page 32 7.1.6.5 */
  {DT_TIMEDIFF                   , 0x00, OC_DEFTYPE  , DT_UNSIGNED32, 0       ,  6}, /* CIA-301 page 32 7.1.6.6 */
  {DT_DOMAIN                     , 0x00, OC_DEFTYPE  , DT_UNSIGNED32, 0       ,  sizeof(struct canopen_objdir_dereftype_s)},
  {DT_INTEGER24                  , 0x00, OC_DEFTYPE  , DT_UNSIGNED32, 0       ,  3},
  {DT_REAL64                     , 0x00, OC_DEFTYPE  , DT_UNSIGNED32, 0       ,  8},
  {DT_INTEGER40                  , 0x00, OC_DEFTYPE  , DT_UNSIGNED32, 0       ,  5},
  {DT_INTEGER48                  , 0x00, OC_DEFTYPE  , DT_UNSIGNED32, 0       ,  6},
  {DT_INTEGER56                  , 0x00, OC_DEFTYPE  , DT_UNSIGNED32, 0       ,  7},
  {DT_INTEGER64                  , 0x00, OC_DEFTYPE  , DT_UNSIGNED32, 0       ,  8},
  {DT_UNSIGNED24                 , 0x00, OC_DEFTYPE  , DT_UNSIGNED32, 0       ,  3},
  {DT_UNSIGNED40                 , 0x00, OC_DEFTYPE  , DT_UNSIGNED32, 0       ,  5},
  {DT_UNSIGNED48                 , 0x00, OC_DEFTYPE  , DT_UNSIGNED32, 0       ,  6},
  {DT_UNSIGNED56                 , 0x00, OC_DEFTYPE  , DT_UNSIGNED32, 0       ,  7},
  {DT_UNSIGNED64                 , 0x00, OC_DEFTYPE  , DT_UNSIGNED32, 0       ,  8},

  {DT_PDO_COMMUNICATION_PARAMETER, 0x00, OC_DEFSTRUCT, DT_UNSIGNED8 , 0       ,  6}, /* Main DEFSTRUCT entry */
  {DT_PDO_COMMUNICATION_PARAMETER, 0x01, OC_DEFSTRUCT, DT_UNSIGNED32, 0       ,  0}, /* COB_ID */
  {DT_PDO_COMMUNICATION_PARAMETER, 0x02, OC_DEFSTRUCT, DT_UNSIGNED8 , 0       ,  0}, /* Transmission type */
  {DT_PDO_COMMUNICATION_PARAMETER, 0x03, OC_DEFSTRUCT, DT_UNSIGNED16, 0       ,  0}, /* Inhibit time */
  {DT_PDO_COMMUNICATION_PARAMETER, 0x04, OC_DEFSTRUCT, DT_UNSIGNED8 , 0       ,  0}, /* reserved */
  {DT_PDO_COMMUNICATION_PARAMETER, 0x05, OC_DEFSTRUCT, DT_UNSIGNED16, 0       ,  0}, /* Event timer */
  {DT_PDO_COMMUNICATION_PARAMETER, 0x06, OC_DEFSTRUCT, DT_UNSIGNED8 , 0       ,  0}, /* SYNC start value */

  {DT_PDO_MAPPING                , 0x00, OC_DEFSTRUCT, DT_UNSIGNED8 , 0       , 64},
  {DT_PDO_MAPPING                , 0x01, OC_DEFSTRUCT, DT_UNSIGNED32, 0       , 64}, /* Entries 0x01-0x40 */

  {DT_SDO_PARAMETER              , 0x00, OC_DEFSTRUCT, DT_UNSIGNED8 , 0       ,  3},
  {DT_SDO_PARAMETER              , 0x01, OC_DEFSTRUCT, DT_UNSIGNED8 , 0       ,  0}, /* COB-ID client->server */
  {DT_SDO_PARAMETER              , 0x02, OC_DEFSTRUCT, DT_UNSIGNED8 , 0       ,  0}, /* COB-ID server->client */
  {DT_SDO_PARAMETER              , 0x03, OC_DEFSTRUCT, DT_UNSIGNED8 , 0       ,  0}, /* Node-ID of SDO's client resp. server*/

  {DT_IDENTITY                   , 0x00, OC_DEFSTRUCT, DT_UNSIGNED8 , 0       ,  4},
  {DT_IDENTITY                   , 0x01, OC_DEFSTRUCT, DT_UNSIGNED32, AA_CONST,  0}, /* Vendor-ID */
  {DT_IDENTITY                   , 0x02, OC_DEFSTRUCT, DT_UNSIGNED32, AA_CONST,  0}, /* Product code */
  {DT_IDENTITY                   , 0x03, OC_DEFSTRUCT, DT_UNSIGNED32, AA_CONST,  0}, /* Revision number */
  {DT_IDENTITY                   , 0x04, OC_DEFSTRUCT, DT_UNSIGNED32, AA_CONST,  0}, /* Serial number */
};

#define CANOPEN_TYPES_COUNT (sizeof(canopen_types)/sizeof(canopen_types[0]))

#define FOREACH_CONTINUE 0
#define FOREACH_BREAK    1

typedef int (canopen_foreach_cb)(FAR struct canopen_objdir_s *objdir,
                                 FAR const struct canopen_var_s *var,
                                 FAR void *env);

struct canopen_cb_browse_env
{
  //internal
  int      arrayitmsize; //item size of current array
  //input
  bool     setup; //TRUE when called from setup, else false
  uint16_t reqindex; // only when setup==FALSE
  uint8_t  reqsubindex; // only when setup==FALSE
  //output
  int      result;
  uint32_t storage;
  int      datasize;
  FAR const struct canopen_var_s *vardef;
};

static int canopen_findobj(FAR struct canopen_objdir_s *objdir,
                           uint16_t index, uint8_t subindex,
                           FAR const struct canopen_var_s **definition,
                           FAR uint32_t *dataoffset, FAR uint32_t *datasize);

/******************************************************************************/
static int canopen_foreachvar(FAR struct canopen_objdir_s *objdir,
                              canopen_foreach_cb callback, FAR void *env)
{
  int i;
  int j;
  int ret;

  for (i=0; i<objdir->numvartables; i++)
    {
      FAR const struct canopen_var_s *vartable = objdir->vartables[i];
      for (j=0;j<objdir->varcounts[i]; j++)
        {
          ret = callback(objdir, vartable+j, env);
          if(ret == FOREACH_BREAK)
            {
              return 0;
            }
        }
    }
  return 0;
}

/******************************************************************************/
static void canopen_objdir_cache_report(FAR struct canopen_objdir_s *objdir)
{
  printf("CACHE: %u hits, %u miss\n", objdir->cache_hit, objdir->cache_miss);
}

/******************************************************************************/
static int canopen_browse_cb(FAR struct canopen_objdir_s *objdir,
                               FAR const struct canopen_var_s *var, FAR void *env)
{
  FAR struct canopen_cb_browse_env *penv = env;
  int ret;

  if (penv->setup)
    {
      printf("id %5d sub %3d type %5d code %d info %d ", var->index, var->subindex, var->type, var->objcode, var->info);
    }

  if (var->objcode == OC_DEFTYPE)
    {
      penv->datasize = 0;
      if (penv->setup) coinfo("DEFTYPE\n");
      /* Check that DEFTYPEs only have a zero subindex */
      if (var->subindex != 0)
        {
          if(penv->setup)
            {
              coerr("id %5d is a DEFTYPE, but has subindex %d !=0\n", var->index, var->subindex);
              penv->result = -EINVAL;
              return FOREACH_BREAK;
            }
        }
    }
  else if (var->objcode == OC_DEFSTRUCT)
    {
      penv->datasize = 0;
      if (penv->setup) coinfo("DEFSTRUCT");
      if(var->subindex==0)
        {
          if (penv->setup) coinfo(" with %d fields\n", var->info);
        }
      else
        {
          if (penv->setup) coinfo("\n");
        }
    }
  else if (var->objcode == OC_VAR)
    {
      FAR const struct canopen_var_s *type;
      ret = canopen_findobj(objdir, var->type, 0, &type, NULL, NULL); //we expect a DEFTYPE
      if (type->objcode != OC_DEFTYPE)
        {
          coerr("var id %d sub %d is not a DEFTYPE!\n", var->type, 0);
          penv->result = -EINVAL;
          return FOREACH_BREAK;
        }
      if (penv->setup) coinfo("Variable with type %d size %d OFFSET %u\n",var->type, type->info, penv->storage);
      penv->datasize = type->info;
      penv->storage += penv->datasize;
    }
  else if (var->objcode == OC_DOMAIN)
    {
    if (penv->setup)
      {
        //a domain var is stored as uint32_t size then uint8_t *ptr (that may contain a path to a file)
        //initialize that to size=0, ptr=null
        penv->datasize = sizeof(uint32_t)+sizeof(uint8_t*);
        penv->storage += penv->datasize;
      }
    }
  else if (var->objcode == OC_ARRAY)
    {
      if(var->subindex==0)
        {
          FAR const struct canopen_var_s *type;
          ret = canopen_findobj(objdir, var->type, 0, &type, NULL, NULL); //we expect a DEFTYPE
          if (type->objcode != OC_DEFTYPE)
            {
              coerr("var id %d sub %d is not a DEFTYPE!\n", var->type, 0);
              penv->result = -EINVAL;
              return FOREACH_BREAK;
            }
          if (penv->setup) coinfo("Array with %d entries of type %d size %d OFFSET %u\n", var->info, var->type, type->info, penv->storage);
          penv->arrayitmsize  = type->info;
          penv->datasize = 0;
        }
      else
        {
          //Subindex not zero: Can be a single entry or a range of entries
          if(var->info==0)
            {
              penv->datasize = penv->arrayitmsize;
              if (penv->setup)
                {
                  coinfo("Entry #%d OFFSET %u\n", var->subindex, penv->storage);
                }
              else
                {
                  if (penv->reqindex == var->index && penv->reqsubindex == var->subindex)
                    {
                      penv->result   = OK;
                      penv->vardef   = var;
                      return FOREACH_BREAK;
                    }
                }
              penv->storage += penv->datasize;
            }  
          else
            {
              int i;
              if (var->info <= var->subindex)
                {
                  coerr("Range is not defined correctly\n");
                  penv->result = -EINVAL;
                  return FOREACH_BREAK;
                }
              if (penv->setup) coinfo("Entries #%d through #%d\n", var->subindex, var->info);
              for (i=var->subindex; i<=var->info; i++)
                {
                  penv->datasize = penv->arrayitmsize;
                  if (penv->setup)
                    {
                      coinfo("Entry #%d OFFSET %u\n", i, penv->storage);
                    }
                  else
                    {
                      if (penv->reqindex == var->index && penv->reqsubindex == i)
                        {
                          penv->result   = OK;
                          penv->vardef   = var;
                          return FOREACH_BREAK;
                        }
                    }
                  penv->storage += penv->datasize;
                }
            }
        }
    }
  else if (var->objcode == OC_RECORD)
    {
      int r,nf;
      FAR const struct canopen_var_s *type;
      if(penv->setup)
        {
          if(var->subindex!=0)
            {
              //Only entry zero is stored in table for records.
              coerr("forbidden subindex for a record!\n");
              penv->result = -EINVAL;
              return FOREACH_BREAK;
            }
        }

      ret = canopen_findobj(objdir, var->type, 0, &type, NULL, NULL);
      if (ret != OK)
        {
          coerr("Search for type failed\n");
          penv->result = -ENOENT;
          return FOREACH_BREAK;
        }
      if (type->objcode != OC_DEFSTRUCT)
        {
          coerr("var id %d sub %d is not a DEFSTRUCT!\n", var->type, 0);
          penv->result = -EINVAL;
          return FOREACH_BREAK;
        }
      nf = type->info;
      if (penv->setup) coinfo("Record with %d fields\n", nf);
      for(r=0; r<nf; r++)
        {
          ret = canopen_findobj(objdir, var->type, r+1, &type, NULL, NULL);
          if (ret != OK)
            {
              coerr("Search for struct field failed\n");
              penv->result = -ENOENT;
              return FOREACH_BREAK;
            }
          if (type->objcode != OC_DEFSTRUCT)
            {
              coerr("var id %d sub %d is not a DEFSTRUCT!\n", var->type, r+1);
              penv->result = -EINVAL;
              return FOREACH_BREAK;
            }
          if (penv->setup) coinfo("Field #%d with type %d ", r+1, type->type);
          ret = canopen_findobj(objdir, type->type, 0, &type, NULL, NULL);
          if (ret != OK)
            {
              coerr("Search for field type failed\n");
              penv->result = -ENOENT;
              return FOREACH_BREAK;
            }
          if (type->objcode != OC_DEFTYPE)
            {
              coerr("var id %d sub %d is not a DEFTYPE!\n", type->type, 0);
              penv->result = -EINVAL;
              return FOREACH_BREAK;
            }
          penv->datasize = type->info;
          if (penv->setup)
            {
              coinfo("size %d OFFSET %u\n", type->info, penv->storage);
            }
          else
            {
              if (penv->reqindex == var->index && penv->reqsubindex == (r+1))
                {
                  penv->result   = OK;
                  penv->vardef   = var;
                  return FOREACH_BREAK;
                }
            }
          penv->storage += penv->datasize;
        } //for each field
    } //record
  else
    {
      //invalid entry type
      if (penv->setup) coinfo("\n");
    }

  if (!penv->setup)
    {
      if (penv->reqindex == var->index && penv->reqsubindex == var->subindex)
        {
          penv->result = OK;
          penv->vardef = var;
          return FOREACH_BREAK;
        }
    }

  

  return FOREACH_CONTINUE;
}

/****************************************************************************
 * Name: canopen_findobj
 *
 * Description:
 *   Find the storage offset of a CAN variable/type. A LRU cache is used to
 *   speed up the search, with an eviction strategy based on entry age,
 *   as described in: https://en.wikipedia.org/wiki/Cache_algorithms#LRU
 *
 * Input Parameters:
 *   objdir     - Pointer to an instance of a CANopen object directory
 *   index      - The CANopen variable idex
 *   subidex    - The CANopen variable subindex.
 *   definition - Pointer to the variable definition
 *   dataoffset - Pointer to the variable storage
 *   datasize   - Size of the variable
 *
 * Returned Value:
 *   Zero if the call was successful, else a negated errno value.
 *
 ****************************************************************************/
static int canopen_findobj(FAR struct canopen_objdir_s *objdir,
                           uint16_t index, uint8_t subindex,
                           FAR const struct canopen_var_s **definition,
                           FAR uint32_t *dataoffset, FAR uint32_t *datasize)
{
  struct canopen_cb_browse_env env;
  int i;
  int maxage   = 0;
  int maxindex = -1;
  bool cached  = false;

  /* Search the variable in the cache */
  for (i=0;i<CONFIG_CANOPEN_CACHESIZE;i++)
    {
      if (objdir->cache[i].index == index && objdir->cache[i].subindex == subindex)
        {
          *definition = objdir->cache[i].definition;
          if (dataoffset) *dataoffset = objdir->cache[i].dataoffset;
          if (datasize  ) *datasize   = objdir->cache[i].datasize;
          cached = true;
        }
      else
        {
          /* saturate age of other cache entries */
          if (objdir->cache[i].age < CANOPEN_OBJDIR_CACHE_AGE_MAX)
            {
              objdir->cache[i].age += 1;
            }
        }
    }

  if(cached)
    {
      printf("cache HIT  for i=%d s=%d\n", index, subindex);
      objdir->cache_hit += 1;
      return OK;
    }

  objdir->cache_miss += 1;
  printf("cache MISS for i=%d s=%d\n", index, subindex);

  /* Enumerate all var tables, also computing data offset */
  env.setup       = 0;
  env.reqindex    = index;
  env.reqsubindex = subindex;
  env.storage     = 0;
  env.datasize    = 0;
  env.vardef      = NULL;

  canopen_foreachvar(objdir, canopen_browse_cb, &env);

  if (env.vardef != NULL && env.result == OK)
    {
      *definition = env.vardef;
      if (dataoffset) *dataoffset = env.storage;
      if (datasize)   *datasize   = env.datasize;
      /* Store the uncached variable in the cache */

      /* find a free entry in the cache AND get the max age */
      for (i=0;i<CONFIG_CANOPEN_CACHESIZE;i++)
        {
          if (objdir->cache[i].age > maxage)
            {
              maxage = objdir->cache[i].age;
              maxindex = i;
            }
        }

      objdir->cache[maxindex].age        = 0;
      objdir->cache[maxindex].index      = index;
      objdir->cache[maxindex].subindex   = subindex;
      objdir->cache[maxindex].definition = env.vardef;
      objdir->cache[maxindex].dataoffset = env.storage;
      objdir->cache[maxindex].datasize   = env.datasize; 
      return OK;
    }

  return env.result;
}

/****************************************************************************
 * Name: canopen_objdirinit
 *
 * Description:
 *   Initialize an empty CANopen object directory, then load it with
 *   standard CANopen data types.
 *
 * Input Parameters:
 *   objdir  - Pointer to an instance of a CANopen object directory
 *
 * Returned Value:
 *   Zero if the call was successful, else a negated errno value.
 *
 ****************************************************************************/

int canopen_objdir_init(FAR struct canopen_objdir_s *objdir)
{
  /* Erase object */
  memset(objdir, 0, sizeof(struct canopen_objdir_s));

  return canopen_objdir_add(objdir, canopen_types, CANOPEN_TYPES_COUNT);
}

/******************************************************************************/
static int canopen_checkdupevars(FAR struct canopen_objdir_s *objdir, uint16_t index, uint8_t subindex)
{
  return 0;
}

/****************************************************************************
 * Name: canopen_objdiradd
 *
 * Description:
 *   Load a CANopen object dictionnary with a list of variable declarations.
 *
 * Input Parameters:
 *   objdir  - Pointer to an instance of a CANopen object directory
 *   vars    - Pointer to an array of variable declarations
 *   count   - Number of entries in the vars array
 *
 * Returned Value:
 *   Zero if the call was successful, else a negated errno value.
 *
 ****************************************************************************/

int canopen_objdir_add(FAR struct canopen_objdir_s *objdir,
                       FAR const struct canopen_var_s *vars, 
                       int count)
{
  int i;
  /* Check that directory has not already been set up */

  /* Check that there is room for this list */

  if (objdir->numvartables >= CONFIG_CANOPEN_VARTABLES)
    {
    coerr("Cannot add this variable list, no free slot\n");
    return -ENOMEM;
    }

  /* Check for duplicate vars with other items in same list */

  /* Check for duplicate vars with other lists */
  for(i=0; i<count; i++)
    {
    canopen_checkdupevars(objdir, vars[i].index, vars[i].subindex);
    }

  /* Store entry in slot */

  objdir->vartables[objdir->numvartables] = vars;
  objdir->varcounts[objdir->numvartables] = count;
  objdir->numvartables += 1;
  coinfo("Stored %d vars, total %d tables\n", count, objdir->numvartables);

  return OK;
}

/****************************************************************************
 * Name: canopen_objdirsetup
 *
 * Description:
 *   Allocate variable storage for all variables declared in this object
 *   directory and make them available for the application. After this call 
 *   is successful, no new variable can be added to the repository.
 *
 * Input Parameters:
 *   objdir  - Pointer to an instance of a CANopen object directory
 *
 * Returned Value:
 *   Zero if the call was successful, else a negated errno value.
 *
 ****************************************************************************/

int canopen_objdir_setup(FAR struct canopen_objdir_s *objdir)
{
  struct canopen_cb_browse_env env;
  FAR void *tmp;

  coinfo("SETUP: START\n");

  /* Check that directory has not already been set up */
  if (objdir->flag_allocated)
    {
      return -EACCES;
    }

  /* Enumerate all var tables */
  env.setup   = 1;
  env.result  = 0;
  env.storage = 0;

  /* Enumerate all vars in each table */
  canopen_foreachvar(objdir, canopen_browse_cb, &env);

  /* Allocate RAM */
  coinfo("Number of bytes to store directory: %u\n", env.storage);
  tmp = malloc(env.storage);
  if(tmp==NULL)
    {
      coerr("Failed to allocate memory\n");
      return -ENOMEM;
    }
  memset(tmp, 0, env.storage);

  canopen_objdir_cache_report(objdir);
  objdir->storage = tmp;
  objdir->flag_allocated = 1;
  return OK;
}

/****************************************************************************
 * Name: canopen_objdirfree
 *
 * Description:
 *   Deallocate all storage affected to vars in this directory. This means
 *   that variable tables can be added again.
 *
 * Input Parameters:
 *   objdir  - Pointer to an instance of a CANopen object directory
 *
 * Returned Value:
 *   Zero if the call was successful, else a negated errno value.
 *
 ****************************************************************************/

int canopen_objdir_free(FAR struct canopen_objdir_s *objdir)
{
  /* Check that directory has already been set up */
 if (objdir->flag_allocated)
    {
      return EACCES;
    }

  /* Free RAM */
  free(objdir->storage);

  /* Mark as not setup */
  objdir->flag_allocated = 0;

  return OK;
}

/****************************************************************************
 * Name: canopen_objdirchecksum
 *
 * Description:
 *   Computes a check sum of all declared variables. This does not depend on the
 *   variable values, only descriptors. This is used to check consistency of
 *   saved variables.
 *
 * Input Parameters:
 *   objdir    - Pointer to an instance of a CANopen object directory
 *   checksum  - Pointer to a buffer to hold the checksum 
 *
 * Returned Value:
 *   Zero if the call was successful, else a negated errno value.
 *
 ****************************************************************************/

int canopen_objdir_checksum(FAR struct canopen_objdir_s *objdir,
                            uint32_t *checksum)
{
  /* Check that directory has already been set up */
 if (objdir->flag_allocated)
    {
      return -EACCES;
    }

  /* Initialize the CRC */

  /* Enumerate all var tables */

  /* Enumerate all vars in each table */

  /* Accumulate data in the CRC */

  return -ENOSYS;
}

/****************************************************************************
 * Name: canopen_objdir_findvar
 *
 * Description:
 *   Find the storage address of a variable, and return info about that
 *   variable. The variable can be a real variable, or a virtual variable that
 *   does not really use storage, eg array length, number of records, type
 *   entries at subindex 0xFF, etc.
 *
 * Input Parameters:
 *   objdir    - Pointer to an instance of a CANopen object directory
 *   index     - The index of the requested variable
 *   subindex  - The subindex of the requested variable
 *   storage   - A pointer to memory holding the variable's value
 *   size      - The number of bytes occupied by this variable
 *   access    - The access rights to the variable
 *
 * Returned Value:
 *   Zero if the call was successful, else a negated errno value.
 *
 *
 ****************************************************************************/
int canopen_objdir_findvar(FAR struct canopen_objdir_s *objdir, 
                           uint16_t index, uint8_t subindex,
                           FAR uint8_t **storage, FAR uint32_t *size,
                           FAR uint16_t *type, FAR uint8_t *access)
{
  FAR const struct canopen_var_s *var;
  int ret;
  uint32_t off,sz;
  uint8_t realsub;

  /* Check that directory has already been set up */
 if (!objdir->flag_allocated)
    {
      return -EACCES;
    }

  realsub = subindex;
  if(subindex == 255)
    {
      //if asking for type entry (sub 255) then ask the main entry instead.
      realsub = 0;
    }

  ret = canopen_findobj(objdir, index, realsub, &var, &off, &sz);
  if (ret != OK)
    {
      coerr("Object not found\n");
      return -ENOENT;
    }

  if (var->objcode == OC_DEFTYPE)
    {
      // Reading a DEFTYPE subindex 0 returns the number of bits to store the type expressed as UNSIGNED32
      uint32_t numbits = var->info;
      if (subindex != 0)
        {
          //only subindex 0 is valid
          return -ENOENT;
        }
      coinfo("Returning the number of bits in a deftype\n");
      //special case for variable length types
      if(var->index == DT_DOMAIN)    numbits=0;
      if(var->index == DT_VISSTRING) numbits=0;
      if(var->index == DT_OCTSTRING) numbits=0;
      if(var->index == DT_UNISTRING) numbits=0;

      numbits <<= 3; //bytes to bits
      objdir->temp[0] = numbits & 0xFF;
      objdir->temp[1] = (numbits>> 8) & 0xFF;
      objdir->temp[2] = (numbits>>16) & 0xFF;
      objdir->temp[3] = (numbits>>24) & 0xFF;
      if (storage) *storage = objdir->temp;
      if (size)    *size    = 4;
      if (type)    *type    = DT_UNSIGNED32;
      if (access)  *access  = AA_RO;
    }
  else if (var->objcode == OC_DEFSTRUCT)
    {
      if (subindex == 0)
        {
          coinfo("Returning the number of fields in a defstruct\n");
          //Subindex 0 is an UNSIGNED8 with the number of fields
          objdir->temp[0] = var->info;
          if (storage) *storage = objdir->temp;
          if (size)    *size    = 1;
          if (type)    *type    = DT_UNSIGNED8;
          if (access)  *access  = AA_RO;
        }
      else
        {
          //Other subindexes returns the type index
          coinfo("Returning the type of a field in a defstruct\n");
          objdir->temp[0] =  var->type     & 0xFF;
          objdir->temp[1] = (var->type>>8) & 0xFF;
          if (storage) *storage = objdir->temp;
          if (size)    *size    = 2;
          if (type)    *type    = DT_UNSIGNED16;
          if (access)  *access  = AA_RO;
        }
    }
  else if (var->objcode == OC_VAR)
    {
      if (subindex == 0xFF)
        {
          //The var definition that was obtained is really entry zero.
          coinfo("Returning the type of a single var\n");
          //Subindex 255 provides the type
          objdir->temp[0] = OC_VAR;
          objdir->temp[1] =  var->type     & 0xFF;
          objdir->temp[2] = (var->type>>8) & 0xFF;
          objdir->temp[3] = 0;
          if (storage) *storage = objdir->temp;
          if (size)    *size    = 4;
          if (type)    *type    = DT_UNSIGNED32;
          if (access)  *access  = AA_RO;
        }
      else if (subindex != 0)
        {
          //only subindex 0 (and 255) is valid
          return -ENOENT;
        }
      else
        {
          coinfo("Returning a real atomic var\n");
          if (storage) *storage = objdir->storage + off;
          if (size)    *size    = sz;
          if (type)    *type    = var->type; 
          if (access)  *access  = var->access;
        }
    }
  else if (var->objcode == OC_DOMAIN)
    {
      if (subindex != 0)
        {
          //only subindex 0 is valid
          return -ENOENT;
        }
      coinfo("Returning info about a domain\n");
      if (storage) *storage = (uint8_t*)(((uint32_t*)objdir->temp)[1]);
      if (size)    *size    = ((uint32_t*)objdir->temp)[0];
      if (type)    *type    = DT_DOMAIN; 
      if (access)  *access  = var->access;
    }
  else if (var->objcode == OC_ARRAY)
    {
      if (subindex == 0)
        {
          coinfo("Returning the number of items in an array\n");
          //Subindex 0 is an UNSIGNED8 with the number of items
          objdir->temp[0] = var->info;
          if (storage) *storage = objdir->temp;
          if (size)    *size    = 1;
          if (type)    *type    = DT_UNSIGNED8;
          if (access)  *access  = AA_RO;
        }
      else if (subindex == 0xFF)
        {
          //The var definition that was obtained is really entry zero.
          coinfo("Returning the type of an array entries\n");
          //Subindex 255 provides the structure
          objdir->temp[0] = OC_ARRAY;
          objdir->temp[1] =  var->type     & 0xFF;
          objdir->temp[2] = (var->type>>8) & 0xFF;
          objdir->temp[3] = 0;
          if (storage) *storage = objdir->temp;
          if (size)    *size    = 4;
          if (type)    *type    = DT_UNSIGNED32;
          if (access)  *access  = AA_RO;
        }
      else
        {
          coinfo("Returning a real array item\n");
          if (storage) *storage = objdir->storage + off;
          if (size)    *size    = sz;
          if (type)    *type    = var->type; 
          if (access)  *access  = var->access;
        }
    }
  else if (var->objcode == OC_RECORD)
    {
      FAR const struct canopen_var_s *rtype;
      ret = canopen_findobj(objdir, var->type, 0, &rtype, NULL, NULL);
      if (ret != OK)
        {
          coerr("Search for type failed\n");
          return -ENOENT;
        }
      if (rtype->objcode != OC_DEFSTRUCT)
        {
          coerr("var id %d sub %d is not a DEFSTRUCT!\n", var->type, 0);
          return -EINVAL;
        }
      if (subindex == 0)
        {
          coinfo("Returning the number of fields in a struct\n");
          //Subindex 0 is an UNSIGNED8 with the number of fields (obtained from the deftype)
          objdir->temp[0] = rtype->info;
          if (storage) *storage = objdir->temp;
          if (size)    *size    = 1;
          if (type)    *type    = DT_UNSIGNED8;
          if (access)  *access  = AA_RO;
        }
      else if (subindex == 0xFF)
        {
          //The var definition that was obtained is really entry zero.
          coinfo("Returning the type of a struct\n");
          //Subindex 255 provides the structure
          objdir->temp[0] = OC_RECORD;
          objdir->temp[1] =  var->type     & 0xFF;
          objdir->temp[2] = (var->type>>8) & 0xFF;
          objdir->temp[3] = 0;
          if (storage) *storage = objdir->temp;
          if (size)    *size    = 4;
          if (type)    *type    = DT_UNSIGNED32;
          if (access)  *access  = AA_RO;
        }
      else
        {
          coinfo("Returning a real struct field\n");
          if (storage) *storage = objdir->storage + off;
          if (size)    *size    = sz;
          if (type)    *type    = var->type; 
          if (access)  *access  = var->access;
        }
    }
  return OK;
}

