/************************************************************************************
 * apps/canutils/libcanopen/canopen_objdir.h
 *
 *   Copyright (C) 2016 Sebastien Lorquet. All rights reserved.
 *   Author: Sebastien Lorquet <sebastien@lorquet.fr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ************************************************************************************/

#ifndef __APPS_CANUTILS_LIBCANOPEN_OBJDIR_H
#define __APPS_CANUTILS_LIBCANOPEN_OBJDIR_H

#include <stdint.h>

/* Number of variable tables that can be managed */
#define CONFIG_CANOPEN_VARTABLES 8

/* Number of cached CAN variables. Uncached variables are slow to access. */
#define CONFIG_CANOPEN_CACHESIZE 16

/* Object Code. Defined in CIA-301 page 89, 7.4.3 */
enum canopen_object_code
{
  OC_NULL      = 0,
  OC_DOMAIN    = 2,
  OC_DEFTYPE   = 5, //does not use storage
  OC_DEFSTRUCT = 6, //does not use storage
  OC_VAR       = 7, //use storage according to type
  OC_ARRAY     = 8, //subindex 0 indicates entry type, other subindexes define ranges (and access conds)
  OC_RECORD    = 9, //subindex 0 does not use storage, other subindexes use storage according to type
};

/* Access Attributes. Defined in CIA-301 page 90, 7.4.5 */
enum canopen_access_attribute
{
  AA_READ  = 1,
  AA_RO    = AA_READ,
  AA_WRITE = 2,
  AA_WO    = AA_WRITE,
  AA_CONST = AA_READ | 4,
  AA_RW    = AA_READ | AA_WRITE
};

/* DEFTYPE indexes. Defined in CIA-301 page 90-91 */
enum canopen_deftype
{
  DT_BOOLEAN    = 0x0001,
  DT_INTEGER8   = 0x0002,
  DT_INTEGER16  = 0x0003,
  DT_INTEGER32  = 0x0004,
  DT_UNSIGNED8  = 0x0005,
  DT_UNSIGNED16 = 0x0006,
  DT_UNSIGNED32 = 0x0007,
  DT_REAL32     = 0x0008,
  DT_VISSTRING  = 0x0009,
  DT_OCTSTRING  = 0x000A,
  DT_UNISTRING  = 0x000B,
  DT_TIMEOFDAY  = 0x000C,
  DT_TIMEDIFF   = 0x000D,
  /* 0x000E is reserved */
  DT_DOMAIN     = 0x000F,
  DT_INTEGER24  = 0x0010,
  DT_REAL64     = 0x0011,
  DT_INTEGER40  = 0x0012,
  DT_INTEGER48  = 0x0013,
  DT_INTEGER56  = 0x0014,
  DT_INTEGER64  = 0x0015,
  DT_UNSIGNED24 = 0x0016,
  /* 0x0017 is reserved */
  DT_UNSIGNED40 = 0x0018,
  DT_UNSIGNED48 = 0x0019,
  DT_UNSIGNED56 = 0x001A,
  DT_UNSIGNED64 = 0x001B,
  /* 0x001C - 0x001F are reserved*/
  DT_PDO_COMMUNICATION_PARAMETER = 0x0020,
  DT_PDO_MAPPING                 = 0x0021,
  DT_SDO_PARAMETER               = 0x0022,
  DT_IDENTITY                    = 0x0023
};

/* This structure describes a CANopen variable declaration.
 * Some storage is allocated for entries of kind:
 * - OC_VAR with subindex 0
 * - OC_ARRAY  with subindex > 0 (OC_ARRAY  subindex zero is entry count, value is not stored)
 * - OC_RECORD with subindex > 0 (OC_RECORD subindex zero is entry count, value is not stored)
 */
struct canopen_var_s
{
  uint16_t index;    /* Variable index on 16 bits */
  uint8_t  subindex; /* Variable subindex, must be zero if single var, must not be 255 */
  uint8_t  objcode;  /* Object code, kind of variable*/
  uint16_t type;     /* Index of type object */
  uint8_t  access;   /* Access rights */
  uint8_t  info;     /* Supplementary info according to objcode:
                      * - OC_VAR/DOMAIN   : Storage size in bytes
                                            For DOMAIN this is sizeof(void*)+sizeof(uint32_t)
                      * - OC_ARRAY        : Subindex 0: Highest valid subindex
                                            Others    : end of subindex range started at this subindex.
                      * - OC_DEFTYPEs     : Subindex 0: Number of bytes to store value (except strings)
                                            Others    : 0
                      * - OC_DEFSTRUCTs   : Subindex 0: Highest valid subindex
                      *                     Others    : 0
                      */
};

/* This structure describes a CANopen variable cache entry, to ensure quick access to common vars */
struct canopen_cache_s
{
  uint16_t                        index;      /* Index of cached entry */
  uint8_t                         subindex;   /* Subindex of cached entry */
  uint32_t                        age;        /* Entry age, to find oldest entries */
  FAR const struct canopen_var_s *definition; /* Pointer to variable definition */
  uint32_t                        dataoffset; /* Offset within canopen_objdir_s->storage */
  uint32_t                        datasize;   /* Number of bytes required by this entry */
};

#define CANOPEN_OBJDIR_CACHE_AGE_MAX (1<<31)

/* This is the type of data which is used for variable length types */
struct canopen_objdir_dereftype_s
{
  uint32_t length;
  FAR uint8_t *data;
};

/* This structure describes a full CANopen object directory */
struct canopen_objdir_s
{
  /* Variable definitions */
  FAR const struct canopen_var_s *vartables[CONFIG_CANOPEN_VARTABLES]; /* Storage for pointers to variable tables */
  FAR uint16_t                    varcounts[CONFIG_CANOPEN_VARTABLES]; /* Storage for entry count of variable tables */
  int                             numvartables;                        /* Number of used entries in prev array */

  /* Cache */
  struct canopen_cache_s          cache[CONFIG_CANOPEN_CACHESIZE];     /* Entry cache */
  uint32_t                        cache_hit;                           /* for statistics */
  uint32_t                        cache_miss;                          /* for statistics */

  /* Storage */
  uint8_t *                       storage;                             /* RAM to store variable data */
  uint8_t                         temp[4];                             /* RAM used to return virtual variable data */

  /* Management */
  uint8_t                         flag_allocated: 1;
};

/****************************************************************************
 * Name: canopen_objdirinit
 *
 * Description:
 *   Initialize an empty CANopen object directory, then load it with
 *   standard CANopen data types.
 *
 * Input Parameters:
 *   objdir  - Pointer to an instance of a CANopen object directory
 *
 * Returned Value:
 *   Zero if the call was successful, else a negated errno value.
 *
 ****************************************************************************/
int canopen_objdir_init(FAR struct canopen_objdir_s *objdir);

/****************************************************************************
 * Name: canopen_objdiradd
 *
 * Description:
 *   Load a CANopen object dictionnary with a list of variable declarations.
 *
 * Input Parameters:
 *   objdir  - Pointer to an instance of a CANopen object directory
 *   vars    - Pointer to an array of variable declarations
 *   count   - Number of entries in the vars array
 *
 * Returned Value:
 *   Zero if the call was successful, else a negated errno value.
 *
 ****************************************************************************/
int canopen_objdir_add(FAR struct canopen_objdir_s *objdir,
                       FAR const struct canopen_var_s *vars, int count);

/****************************************************************************
 * Name: canopen_objdirsetup
 *
 * Description:
 *   Allocate variable storage for all variables declared in this object
 *   directory and make them available for the application. After this call 
 *   is successful, no new variable can be added to the repository.
 *
 * Input Parameters:
 *   objdir  - Pointer to an instance of a CANopen object directory
 *
 * Returned Value:
 *   Zero if the call was successful, else a negated errno value.
 *
 ****************************************************************************/
int canopen_objdir_setup(FAR struct canopen_objdir_s *objdir);

/****************************************************************************
 * Name: canopen_objdirfree
 *
 * Description:
 *   Deallocate all storage affected to vars in this directory. This means
 *   that variable tables can be added again.
 *
 * Input Parameters:
 *   objdir  - Pointer to an instance of a CANopen object directory
 *
 * Returned Value:
 *   Zero if the call was successful, else a negated errno value.
 *
 ****************************************************************************/
int canopen_objdir_free(FAR struct canopen_objdir_s *objdir);

/****************************************************************************
 * Name: canopen_objdirchecksum
 *
 * Description:
 *   Computes a check sum of all declared variables. This does not depend on the
 *   variable values, only descriptors. This is used to check consistency of
 *   saved variables.
 *
 * Input Parameters:
 *   objdir    - Pointer to an instance of a CANopen object directory
 *   checksum  - Pointer to a buffer to hold the checksum 
 *
 * Returned Value:
 *   Zero if the call was successful, else a negated errno value.
 *
 ****************************************************************************/
int canopen_objdir_checksum(FAR struct canopen_objdir_s *objdir, uint32_t *checksum);

/****************************************************************************
 * Name: canopen_objdir_findvar
 *
 * Description:
 *   Find the storage address of a variable, and return info about that
 *   variable. The variable can be a real variable, or a virtual variable that
 *   does not really use storage, eg array length, number of records, type
 *   entries at subindex 0xFF, etc.
 *
 * Input Parameters:
 *   objdir    - Pointer to an instance of a CANopen object directory
 *   index     - The index of the requested variable
 *   subindex  - The subindex of the requested variable
 *   storage   - A pointer to memory holding the variable's value
 *   size      - The number of bytes occupied by this variable
 *   access    - The access rights to the variable
 *
 * Returned Value:
 *   Zero if the call was successful, else a negated errno value.
 *
 *
 ****************************************************************************/
int canopen_objdir_findvar(FAR struct canopen_objdir_s *objdir, 
                           uint16_t index, uint8_t subindex,
                           FAR uint8_t **storage, FAR uint32_t *size,
                           FAR uint16_t *type, FAR uint8_t *access);

/****************************************************************************
 * Name: canopen_objdirload
 *
 * Description:
 *   Backup the contents of a CANopen object directory to a file descriptor.
 *   The file descriptor can refer to an open file in a writable filesystem,
 *   a network socket, or a character device, such as an eeprom.
 *
 * Input Parameters:
 *   objdir  - Pointer to an instance of a CANopen object directory
 *   fd      - A file descriptor opened in write mode
 *
 * Returned Value:
 *   Zero if the call was successful, else a negated errno value.
 *
 ****************************************************************************/
int canopen_objdir_load(FAR struct canopen_objdir_s *objdir, int fd);

/****************************************************************************
 * Name: canopen_objdirsave
 *
 * Description:
 *   Backup the contents of a CANopen object directory to a file descriptor.
 *   The file descriptor can refer to an open file in a writable filesystem,
 *   a network socket, or a character device, such as an eeprom.
 *
 * Input Parameters:
 *   objdir  - Pointer to an instance of a CANopen object directory
 *   fd      - A file descriptor opened in write mode
 *
 * Returned Value:
 *   Zero if the call was successful, else a negated errno value.
 *
 ****************************************************************************/
int canopen_objdir_save(FAR struct canopen_objdir_s *objdir, int fd);

#endif /* __APPS_CANUTILS_LIBCANOPEN_OBJDIR_H */

