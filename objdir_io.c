#include <stdlib.h>

#include "canutils/libcanopen/objdir.h"

/****************************************************************************
 * Name: canopen_objdirload
 *
 * Description:
 *   Backup the contents of a CANopen object directory to a file descriptor.
 *   The file descriptor can refer to an open file in a writable filesystem,
 *   a network socket, or a character device, such as an eeprom. The saved
 *   data has the following structure:
 *   - 1 byte: version: must be 0x01
 *   Version 1 format:
 *   - 4 bytes: dictionary checksum
 *   - 2 bytes: Number of variables
 *   Then for each var:
 *   - 2 bytes : index
 *   - 1 byte  : subindex
 *   - N bytes : value, length depends on variable descriptor
 *
 * The checksum is the CRC32 of all concatenated variable descriptors. It is used to ensure
 * that the saved data is consistent with the current dictionary structure.
 *
 * Input Parameters:
 *   objdir  - Pointer to an instance of a CANopen object directory
 *   fd      - A file descriptor opened in write mode
 *
 * Returned Value:
 *   Zero if the call was successful, else a negated errno value.
 *
 ****************************************************************************/
int canopen_objdirload(FAR struct canopen_objdir_s *objdir, int fd)
{
  return ERROR;
}
/****************************************************************************
 * Name: canopen_objdirsave
 *
 * Description:
 *   Backup the contents of a CANopen object directory to a file descriptor.
 *   The file descriptor can refer to an open file in a writable filesystem,
 *   a network socket, or a character device, such as an eeprom.
 *
 * Input Parameters:
 *   objdir  - Pointer to an instance of a CANopen object directory
 *   fd      - A file descriptor opened in write mode
 *
 * Returned Value:
 *   Zero if the call was successful, else a negated errno value.
 *
 ****************************************************************************/
int canopen_objdirsave(FAR struct canopen_objdir_s *objdir, int fd)
{
  return ERROR;
}
