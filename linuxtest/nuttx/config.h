#define CONFIG_BUILD_KERNEL

#include <stdint.h>

#define FAR

#define OK 0
#define ERROR -1

#define set_errno(val) do { errno = (val); } while(0)

