#ifndef __DEBUG
#define __DEBUG

#include <stdio.h>

#define canerr(fmt,...) printf(fmt,##__VA_ARGS__)
#define caninfo(fmt,...) printf(fmt,##__VA_ARGS__)

#endif

