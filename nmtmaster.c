#include <stdlib.h>
#include <errno.h>
#include <debug.h>

#include "canutils/libcanopen/master.h"
#include "canutils/libcanopen/nmt.h"

/* -------------------------------------------------------------------------- */
int canopen_nmtmaster_init(FAR struct canopen_master_s *master)
{
  coinfo("Init NMT master\n");
  return 0;
}

/* -------------------------------------------------------------------------- */
static int canopen_nmtmaster_tx(FAR struct canopen_master_s *master, uint8_t nodeid, uint8_t cmd)
{
  struct canmsg_s msg;
  msg.data[0] = cmd;
  msg.data[1] = nodeid;
  msg.id  = 0;
  msg.dlc = 2;
  return canopen_master_tx(master,&msg);
}

/* -------------------------------------------------------------------------- */
int canopen_nmtmaster_resetapp(FAR struct canopen_master_s *master, int nodeid)
{
  if (!nodeid)
    {
      coinfo("Reset all nodes\n");
    }
  else
    {
      coinfo("Reset node %d\n", nodeid);
    }

  return canopen_nmtmaster_tx(master, nodeid, NMT_CMD_RESET_APP);
}

/* -------------------------------------------------------------------------- */
int canopen_nmtmaster_resetcomm(FAR struct canopen_master_s *master, int nodeid)
{
  if (!nodeid)
    {
      coinfo("Reset-comm all nodes\n");
    }
  else
    {
      coinfo("Reset-comm node %d\n", nodeid);
    }

  return canopen_nmtmaster_tx(master, nodeid, NMT_CMD_RESET_COMM);
}

/* -------------------------------------------------------------------------- */
int canopen_nmtmaster_preop(FAR struct canopen_master_s *master, int nodeid)
{
  if (!nodeid)
    {
      coinfo("Pre-op all nodes\n");
    }
  else
    {
      coinfo("Pre-op node %d\n", nodeid);
    }

  return canopen_nmtmaster_tx(master, nodeid, NMT_CMD_PREOP);
}

/* -------------------------------------------------------------------------- */
int canopen_nmtmaster_start(FAR struct canopen_master_s *master, int nodeid)
{
  if (!nodeid)
    {
      coinfo("Start all nodes\n");
    }
  else
    {
      coinfo("Start node %d\n", nodeid);
    }

  return canopen_nmtmaster_tx(master, nodeid, NMT_CMD_START);
}

/* -------------------------------------------------------------------------- */
int canopen_nmtmaster_stop(FAR struct canopen_master_s *master, int nodeid)
{
  if (!nodeid)
    {
      coinfo("Stop all nodes\n");
    }
  else
    {
      coinfo("Stop node %d\n", nodeid);
    }

  return canopen_nmtmaster_tx(master, nodeid, NMT_CMD_STOP);
}

/* -------------------------------------------------------------------------- */
int canopen_nmtmaster_getstate(FAR struct canopen_master_s *node, int nodeid, FAR uint8_t *state)
{
  return -EINVAL;
}

