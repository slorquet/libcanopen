#ifndef __CANOPEN_SDOSERVER_H__
#define __CANOPEN_SDOSERVER_H__

#include <semaphore.h>

#include "canutils/libcanopen/canopen.h"

struct canopen_slave_s;

/* The server side callback is called only after the transaction is complete */
typedef int (*canopen_sdoserver_varupdated_f)(FAR struct canopen_slave_s *slave, FAR void *ctx,
                                              uint16_t index, uint8_t subindex,
                                              FAR uint8_t *data, uint32_t datalen);

struct canopen_sdoserver_s
{
  uint32_t      totlen; //total length of current transfer
  uint32_t      curoff; //current offset within current transfer
  FAR uint8_t  *varloc; //memory location of current variable
  uint8_t       pending; //type of pending transfer
  uint16_t      pending_index;
  uint8_t       pending_subindex;
  bool          expected_toggle;
  FAR void *    cb_ctx;
  CODE canopen_sdoserver_varupdated_f callback;
};

int canopen_sdoserver_init(FAR struct canopen_sdoserver_s *server,
                           CODE canopen_sdoserver_varupdated_f cb);

int canopen_sdoserver_rxcan(FAR struct canopen_slave_s *node, FAR struct canmsg_s *msg);

#endif // __CANOPEN_SDOSERVER_H__
