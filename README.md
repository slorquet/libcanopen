libcanopen

CANOpen library for NuttX

by Sebastien Lorquet

This library implements the CANOpen communication profile.

By running this software, the application can behave either as a
CANOpen slave or a CANOpen master.

DONE

 * Packet reception and dispatch
 * Extensible Object directory
 * Generic predefined connection set
 * SDO server and client

TODO

 * NMT
 * PDO
 * SYNC
 * EMCY
 * Node guarding