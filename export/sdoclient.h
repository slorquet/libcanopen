#ifndef __CANOPEN_SDOCLIENT_H__
#define __CANOPEN_SDOCLIENT_H__

#include <semaphore.h>

#include "canutils/libcanopen/canopen.h"

struct canopen_master_s;

struct canopen_sdoclient_s
{
  uint8_t         sdoc_pending; //type of pending transfer, used to enable sdo message reception
  uint8_t         sdoc_pendingid; //node id of slave for pending transfer
  sem_t           sdoc_rxsem;   //response semaphore, posted by message reception thread
  struct canmsg_s sdoc_rsp;     //response message
  int             sdoc_timeout; //number of seconds to wait for the response
};

enum
{
  SDOCLIENT_CB_EVENT_START,   /* data=null,  datalen=total len */
  SDOCLIENT_CB_EVENT_DATA,    /* data=valid, datalen=chunk len */
  SDOCLIENT_CB_EVENT_COMPLETE,/* data=null,  datalen=0 */
  SDOCLIENT_CB_EVENT_ABORT    /* data=(uint32_t*)abortcode, datalen=sizeof(uint32_t) */
};

typedef int (*canopen_upload_fragment_f)(FAR struct canopen_master_s *master, FAR void *context,
                                         uint16_t index, uint8_t subindex,
                                         int event, FAR uint8_t *data, uint32_t datalen);

int canopen_sdoclient_init(FAR struct canopen_sdoclient_s *cli, FAR struct canopen_master_s *parent);

int canopen_sdoclient_upload(FAR struct canopen_master_s *master, int tgtid,
                             uint16_t index, uint8_t subindex,
                             CODE canopen_upload_fragment_f callback, FAR void *context);

int canopen_sdoclient_download(FAR struct canopen_master_s *master, int tgtid,
                               uint16_t index, uint8_t subindex, bool expedited,
                               FAR uint8_t *data, uint32_t datalen);

#endif // __CANOPEN_SDOCLIENT_H__
