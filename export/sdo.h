#ifndef __CANOPEN_SDO_H__
#define __CANOPEN_SDO_H__

#include "canutils/libcanopen/canopen.h"

#define CANOPEN_SDO_COMMAND_SHIFT 5
#define CANOPEN_SDO_COMMAND_MASK  (7 << CANOPEN_SDO_COMMAND_SHIFT)
#define CANOPEN_SDO_COMMAND_DNS   (0 << CANOPEN_SDO_COMMAND_SHIFT) /* SDO Download segment, server receives data */
#define CANOPEN_SDO_COMMAND_DNI   (1 << CANOPEN_SDO_COMMAND_SHIFT) /* SDO Download initiate */
#define CANOPEN_SDO_COMMAND_UPI   (2 << CANOPEN_SDO_COMMAND_SHIFT) /* SDO Upload initiate */
#define CANOPEN_SDO_COMMAND_UPS   (3 << CANOPEN_SDO_COMMAND_SHIFT) /* SDO Upload segment, server sends data */
#define CANOPEN_SDO_COMMAND_ABORT (5 << CANOPEN_SDO_COMMAND_SHIFT) /* SDO abort transfer*/

#define CANOPEN_SDO_RESPONSE_DNS   (1 << CANOPEN_SDO_COMMAND_SHIFT) /* SDO Download segment, server receives data */
#define CANOPEN_SDO_RESPONSE_DNI   (3 << CANOPEN_SDO_COMMAND_SHIFT) /* SDO Download initiate */
#define CANOPEN_SDO_RESPONSE_UPI   (2 << CANOPEN_SDO_COMMAND_SHIFT) /* SDO Upload initiate */
#define CANOPEN_SDO_RESPONSE_UPS   (0 << CANOPEN_SDO_COMMAND_SHIFT) /* SDO Upload segment, server sends data */

#define CANOPEN_SDO_INIT_SIZEIND               0x01
#define CANOPEN_SDO_INIT_EXPEDITED             0x02
#define CANOPEN_SDO_INIT_ULEN_SHIFT          2
#define CANOPEN_SDO_INIT_ULEN_MASK           (3 << CANOPEN_SDO_INIT_ULEN_SHIFT)

#define CANOPEN_SDO_SEG_CONTINUE               0x01
#define CANOPEN_SDO_SEG_ULEN_SHIFT             1
#define CANOPEN_SDO_SEG_ULEN_MASK              (7 << CANOPEN_SDO_SEG_ULEN_SHIFT)
#define CANOPEN_SDO_SEG_TOGGLE                 0x10

/* Abort codes */

#define CANOPEN_SDOABORT_NO_TOGGLE        0x05030000lu /* Toggle bit not alternated. */
#define CANOPEN_SDOABORT_TIMEOUT          0x05040000lu /* SDO protocol timed out. */
#define CANOPEN_SDOABORT_INVALID_COMMAND  0x05040001lu /* Client/server command specifier not valid or unknown. */
#define CANOPEN_SDOABORT_INVALID_SIZE     0x05040002lu /* Invalid block size (block mode only). */
#define CANOPEN_SDOABORT_INVALID_SEQ      0x05040003lu /* Invalid sequence number (block mode only). */
#define CANOPEN_SDOABORT_CRC_ERROR        0x05040004lu /* CRC error (block mode only). */
#define CANOPEN_SDOABORT_ENOMEM           0x05040005lu /* Out of memory. */
#define CANOPEN_SDOABORT_EACCESS          0x06010000lu /* Unsupported access to an object. */
#define CANOPEN_SDOABORT_EACCESS_WO       0x06010001lu /* Attempt to read a write only object. */
#define CANOPEN_SDOABORT_EACCESS_RO       0x06010002lu /* Attempt to write a read only object. */
#define CANOPEN_SDOABORT_NO_OBJECT        0x06020000lu /* Object does not exist in the object dictionary. */
#define CANOPEN_SDOABORT_PDO_NOT_MAPPABLE 0x06040041lu /* Object cannot be mapped to the PDO. */
#define CANOPEN_SDOABORT_PDO_TOO_BIG      0x06040042lu /* The number and length of the objects to be mapped would exceed PDO length. */
#define CANOPEN_SDOABORT_EPARM            0x06040043lu /* General parameter incompatibility reason. */
#define CANOPEN_SDOABORT_INTERNAL_ERROR   0x06040047lu /* General internal incompatibility in the device. */
#define CANOPEN_SDOABORT_EACCESS_HWERROR  0x06060000lu /* Access failed due to an hardware error. */
#define CANOPEN_SDOABORT_BAD_LENGTH       0x06070010lu /* Data type does not match, length of service parameter does not match */
#define CANOPEN_SDOABORT_BAD_LENGTH_LONG  0x06070012lu /* Data type does not match, length of service parameter too high */
#define CANOPEN_SDOABORT_BAD_LENGTH_SHORT 0x06070013lu /* Data type does not match, length of service parameter too low */
#define CANOPEN_SDOABORT_NO_SUBINDEX      0x06090011lu /* Sub-index does not exist. */
#define CANOPEN_SDOABORT_EINVAL           0x06090030lu /* Invalid value for parameter (download only). */
#define CANOPEN_SDOABORT_EINVAL_HIGH      0x06090031lu /* Value of parameter written too high (download only). */
#define CANOPEN_SDOABORT_EINVAL_LOW       0x06090032lu /* Value of parameter written too low (download only). */
#define CANOPEN_SDOABORT_EINVAL_MAXMIN    0x06090036lu /* Maximum value is less than minimum value. */
#define CANOPEN_SDOABORT_NOT_AVAIL        0x060A0023lu /* Resource not available: SDO connection */
#define CANOPEN_SDOABORT_GENERAL          0x08000000lu /* General error */
#define CANOPEN_SDOABORT_CANT_STORE       0x08000020lu /* Data cannot be transferred or stored to the application. */
#define CANOPEN_SDOABORT_CANT_STORE_LOC   0x08000021lu /* Data cannot be transferred or stored to the application because of local control. */
#define CANOPEN_SDOABORT_BAD_DEV_STATE    0x08000022lu /* Data cannot be transferred or stored to the application because of the present device state. */
#define CANOPEN_SDOABORT_OBJDIR_FAILURE   0x08000023lu /* Object dictionary dynamic generation fails or no object dictionary is present
                                                          (e.g. object dictionary is generated from file and generation fails because of an file error). */
#define CANOPEN_SDOABORT_NO_DATA          0x08000024lu /* No data available */

enum
{
  SDO_PENDING_NONE,
  SDO_PENDING_DOWNLOAD,
  SDO_PENDING_UPLOAD
};

#endif // __CANOPEN_SDO_H__
