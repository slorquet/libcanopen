#ifndef __NUTTX_DRIVERS_CAN_H__
#define __NUTTX_DRIVERS_CAN_H__

struct can_hdr_s
{
  int ch_id;
  int ch_dlc;
};

struct can_msg_s
{
  struct can_hdr_s cm_hdr;
  uint8_t cm_data[8];
};

#endif // __NUTTX_DRIVERS_CAN_H__
