#include <stdio.h>

#include "nuttx/config.h"

#include "canutils/libcanopen/node.h"
#include "canutils/libcanopen/objdir.h"

struct canopen_node_s node;

void caninfodumpbuffer(char *msg, uint8_t *data, int len)
{
  int i;
  printf("%s\n", msg);

  for (i=0; i<len; i++)
    {
      if ( (i&15)==0) printf("%p: ",(data+i));
      printf("%02X ", data[i]);
      if ( (i&15)==15) printf("\n");
    }
}

/******************************************************************************/
int txcb(FAR void *priv, FAR struct can_msg_s *msg)
{
  printf("tx msg dlc %d canid %d\n", msg->cm_hdr.ch_dlc, msg->cm_hdr.ch_id);
  return 0;
}

/******************************************************************************/
int main(int c, char **v)
{
  int ret;
  ret = canopen_nodeinit(&node, 42, txcb, NULL);

  canopen_nodestop(&node);
  return 0;
}
