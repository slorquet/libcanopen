/************************************************************************************
 * apps/canutils/libcanopen/slave.h
 *
 *   Copyright (C) 2016 Sebastien Lorquet. All rights reserved.
 *   Author: Sebastien Lorquet <sebastien@lorquet.fr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ************************************************************************************/

#ifndef __APPS_CANUTILS_LIBCANOPEN_SLAVE_H
#define __APPS_CANUTILS_LIBCANOPEN_SLAVE_H

#include "canutils/libcanopen/canopen.h"
#include "canutils/libcanopen/objdir.h"
#include "canutils/libcanopen/sdoserver.h"

struct canopen_slave_s
{
  uint8_t id; /* CANopen Node ID in range 1..127 */
  uint8_t nmtstate;
  canopen_txcb_f tx;
  FAR void *tx_priv;

  struct canopen_objdir_s objdir;
  struct canopen_sdoserver_s sdoserver;
};

int canopen_slave_init(FAR struct canopen_slave_s *slave, int nodeid, canopen_txcb_f txcb, FAR void *txpriv);
int canopen_slave_stop(FAR struct canopen_slave_s *slave);
int canopen_slave_rx(FAR struct canopen_slave_s *slave, FAR struct canmsg_s *msg);
int canopen_slave_tx(FAR struct canopen_slave_s *slave, FAR struct canmsg_s *msg);

#endif /* __APPS_CANUTILS_LIBCANOPEN_CANOPEN_OBJDIR_H */

