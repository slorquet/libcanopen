/* This is a CANopen master node */

#include "nuttx/config.h"

#include <stdio.h>
#include <string.h>
#include <debug.h>
#include <errno.h>
#include <semaphore.h>

#include "canutils/libcanopen/canopen.h"
#include "canutils/libcanopen/master.h"
#include "canutils/libcanopen/sdo.h"

/****************************************************************************
 * canopen_master_init
 * Description:
 *   Initialize the CANopen master node.
 ****************************************************************************/
int canopen_master_init(FAR struct canopen_master_s *master, canopen_txcb_f txcb, FAR void *txpriv)
{
  int ret;
  coinfo("Master Node init\n");

  master->tx      = txcb;
  master->tx_priv = txpriv;

  /* Prepare the sdo client */
  ret = canopen_sdoclient_init(&master->sdocli, master);
  if(ret != OK)
    {
      coerr("Failed to initialize SDO client\n");
      return ret;
    }

  return OK;
}

/****************************************************************************
 * canopen_master_stop
 * Description:
 *   Stop the CANopen node.
 ****************************************************************************/
int canopen_master_stop(FAR struct canopen_master_s *master)
{
  coinfo("Master Node stop\n");
  return OK;
}

/****************************************************************************
 * canopen_tx
 * Description:
 *   Main CAN message emitter, using user callback
 ****************************************************************************/
int canopen_master_tx(FAR struct canopen_master_s *master, FAR struct canmsg_s *msg)
{
  return master->tx(master->tx_priv, msg);
}

/****************************************************************************
 * canopen_rxcan
 * Description:
 *   Main CAN message dispatcher. Must be called in a separate thread.
 ****************************************************************************/
int canopen_master_rx(FAR struct canopen_master_s *master, FAR struct canmsg_s *msg)
{
  int id = msg->id;
  char hex[2*8+1];
  int i;
  for(i=0;i<msg->dlc;i++)
    {
      sprintf(hex+(i<<1), "%02X", msg->data[i]);
    }

  if (id == 0) /* NMT slave to master */
    {
      coinfo("NMT    [%3X] L=%d %s\n", id, i, hex);
    }
  else if (master->sdocli.sdoc_pending != SDO_PENDING_NONE)
    {
      if (id == CANOPEN_CANIDBASE_SDORX + master->sdocli.sdoc_pendingid) /* SDO RX */
        {
          coinfo("SDO S>M [%3X] L=%d %s\n", id, i, hex);
          memcpy(&master->sdocli.sdoc_rsp, msg, sizeof(struct canmsg_s));
          sem_post(&master->sdocli.sdoc_rxsem);
        }
    }
  /* 0x700+id node guarding/heartbeat/bootup */
  else
    {
      coinfo("RX CAN MSG! (Unmanaged)\n");
    }
  return OK;
}

