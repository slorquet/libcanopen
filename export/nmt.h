#ifndef __CANOPEN_NMTSLAVE_H__
#define __CANOPEN_NMTSLAVE_H__

#include "canutils/libcanopen/canopen.h"
#include "canutils/libcanopen/slave.h"

enum {
  NMT_STATE_INITIALIZING = 1,
  NMT_STATE_RESET_APPLICATION,
  NMT_STATE_RESET_COMMUNICATION,
  NMT_STATE_PRE_OPERATIONAL,
  NMT_STATE_OPERATIONAL,
  NMT_STATE_STOPPED
};

#define NMT_CMD_START      1   /* cia-301 7.2.8.3.1.1 fig 39 page 77 */
#define NMT_CMD_STOP       2   /* cia-301 7.2.8.3.1.2 fig 40 page 77 */
#define NMT_CMD_PREOP      128 /* cia-301 7.2.8.3.1.3 fig 41 page 78 */
#define NMT_CMD_RESET_APP  129 /* cia-301 7.2.8.3.1.4 fig 42 page 78 */
#define NMT_CMD_RESET_COMM 130 /* cia-301 7.2.8.3.1.5 fig 43 page 78 */

struct canopen_slave_s;

int canopen_nmtslave_init(FAR struct canopen_slave_s *slave);
int canopen_nmtslave_rxcan(FAR struct canopen_slave_s *slave, FAR struct canmsg_s *msg);
int canopen_nmtslave_getstate(FAR struct canopen_slave_s *slave, FAR uint8_t *state);
int canopen_nmtslave_setstate(FAR struct canopen_slave_s *slave, uint8_t deststate);

struct canopen_master_s;

int canopen_nmtmaster_init(FAR struct canopen_master_s *nmt);
int canopen_nmtmaster_resetapp (FAR struct canopen_master_s *master, int nodeid);
int canopen_nmtmaster_resetcomm(FAR struct canopen_master_s *master, int nodeid);
int canopen_nmtmaster_preop    (FAR struct canopen_master_s *master, int nodeid);
int canopen_nmtmaster_start    (FAR struct canopen_master_s *master, int nodeid);
int canopen_nmtmaster_stop     (FAR struct canopen_master_s *master, int nodeid);

int canopen_nmtmaster_getstate(FAR struct canopen_master_s *node, int nodeid, FAR uint8_t *state);

#endif // __CANOPEN_NMT_H__
