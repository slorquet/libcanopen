#include <stdint.h>
#include <string.h>

#include <nuttx/config.h>
#include <debug.h>
#include <errno.h>
#include <time.h>

#include "canutils/libcanopen/master.h"
#include "canutils/libcanopen/objdir.h"
#include "canutils/libcanopen/sdo.h"
#include "canutils/libcanopen/sdoclient.h"

/* -------------------------------------------------------------------------- */
int canopen_sdoclient_init(FAR struct canopen_sdoclient_s *cli, FAR struct canopen_master_s *parent)
{
  cli->sdoc_timeout = 10; //default 10 seconds timeout
  sem_init(&cli->sdoc_rxsem, 0, 0);
  return 0;
}

/* -------------------------------------------------------------------------- */
const char *canopen_sdo_whyabort(uint32_t reason)
{
  const char *r;
  switch(reason)
    {
      case CANOPEN_SDOABORT_NO_TOGGLE       : r = "NO_TOGGLE"       ; break;
      case CANOPEN_SDOABORT_TIMEOUT         : r = "TIMEOUT"         ; break;
      case CANOPEN_SDOABORT_INVALID_COMMAND : r = "INVALID_COMMAND" ; break;
      case CANOPEN_SDOABORT_INVALID_SIZE    : r = "INVALID_SIZE"    ; break;
      case CANOPEN_SDOABORT_INVALID_SEQ     : r = "INVALID_SEQ"     ; break;
      case CANOPEN_SDOABORT_CRC_ERROR       : r = "CRC_ERROR"       ; break;
      case CANOPEN_SDOABORT_ENOMEM          : r = "ENOMEM"          ; break;
      case CANOPEN_SDOABORT_EACCESS         : r = "EACCESS"         ; break;
      case CANOPEN_SDOABORT_EACCESS_WO      : r = "EACCESS_WO"      ; break;
      case CANOPEN_SDOABORT_EACCESS_RO      : r = "EACCESS_RO"      ; break;
      case CANOPEN_SDOABORT_NO_OBJECT       : r = "NO_OBJECT"       ; break;
      case CANOPEN_SDOABORT_PDO_NOT_MAPPABLE: r = "PDO_NOT_MAPPABLE"; break;
      case CANOPEN_SDOABORT_PDO_TOO_BIG     : r = "PDO_TOO_BIG"     ; break;
      case CANOPEN_SDOABORT_EPARM           : r = "EPARM"           ; break;
      case CANOPEN_SDOABORT_INTERNAL_ERROR  : r = "INTERNAL_ERROR"  ; break;
      case CANOPEN_SDOABORT_EACCESS_HWERROR : r = "EACCESS_HWERROR" ; break;
      case CANOPEN_SDOABORT_BAD_LENGTH      : r = "BAD_LENGTH"      ; break;
      case CANOPEN_SDOABORT_BAD_LENGTH_LONG : r = "BAD_LENGTH_LONG" ; break;
      case CANOPEN_SDOABORT_BAD_LENGTH_SHORT: r = "BAD_LENGTH_SHORT"; break;
      case CANOPEN_SDOABORT_NO_SUBINDEX     : r = "NO_SUBINDEX"     ; break;
      case CANOPEN_SDOABORT_EINVAL          : r = "EINVAL"          ; break;
      case CANOPEN_SDOABORT_EINVAL_HIGH     : r = "EINVAL_HIGH"     ; break;
      case CANOPEN_SDOABORT_EINVAL_LOW      : r = "EINVAL_LOW"      ; break;
      case CANOPEN_SDOABORT_EINVAL_MAXMIN   : r = "EINVAL_MAXMIN"   ; break;
      case CANOPEN_SDOABORT_NOT_AVAIL       : r = "NOT_AVAIL"       ; break;
      case CANOPEN_SDOABORT_GENERAL         : r = "GENERAL"         ; break;
      case CANOPEN_SDOABORT_CANT_STORE      : r = "CANT_STORE"      ; break;
      case CANOPEN_SDOABORT_CANT_STORE_LOC  : r = "CANT_STORE_LOC"  ; break;
      case CANOPEN_SDOABORT_BAD_DEV_STATE   : r = "BAD_DEV_STATE"   ; break;
      case CANOPEN_SDOABORT_OBJDIR_FAILURE  : r = "OBJDIR_FAILURE"  ; break;
      case CANOPEN_SDOABORT_NO_DATA         : r = "NO_DATA"         ; break;
      default                               : r = "(Unknown)"       ;
    }
  return r;
}

/* -------------------------------------------------------------------------- */
/* Send Download Initiate [Expedited] */
static int canopen_sdocli_send_dli(FAR struct canopen_master_s *master, int tgtid,
                                   uint16_t index, uint8_t subindex,
                                   bool expedited, FAR uint8_t *data, int datalen)
{
  struct canmsg_s msg;
  int unused;

  if(datalen==0)
    {
      coerr("error: zero bytes in download\n");
      return -EINVAL;
    }

  msg.data[0] = CANOPEN_SDO_COMMAND_DNI | CANOPEN_SDO_INIT_SIZEIND;
  msg.data[0] &= ~CANOPEN_SDO_INIT_ULEN_MASK;

  msg.data[1] = (uint8_t)(index & 0xFF);
  msg.data[2] = (uint8_t)(index >> 8);
  msg.data[3] = (uint8_t) subindex;

  if (expedited)
    {
      if (datalen>4)
        {
          coerr("error: more than 4 bytes in download init exp\n");
          return -EINVAL;
        }
      unused = 4 - datalen;
      msg.data[0] |= CANOPEN_SDO_INIT_EXPEDITED;
      msg.data[0] |= unused << CANOPEN_SDO_INIT_ULEN_SHIFT;

      //bytes 4..7 encode data
      memcpy(msg.data+4, data, datalen);
      memset(msg.data+4+datalen, 0, unused);
    }
  else
    {
      //bytes 4..7 encode data size
      msg.data[4] =  datalen      & 0xFF;
      msg.data[5] = (datalen>> 8) & 0xFF;
      msg.data[6] = (datalen>>16) & 0xFF;
      msg.data[7] = (datalen>>24) & 0xFF;
    }

  msg.id  = 0x580 + tgtid;
  msg.dlc = 8;

  return canopen_master_tx(master, &msg);
}

/* -------------------------------------------------------------------------- */
static int canopen_sdocli_send_dns(FAR struct canopen_master_s *master, int tgtid ,
                            bool toggle, bool cont,
                            FAR uint8_t *data, int datalen)
{
  struct canmsg_s msg;
  int unused;

  if(datalen==0)
    {
      coerr("error: zero bytes in download seg\n");
      return -EINVAL;
    }
  if(datalen > 7)
    {
      coerr("error: more than 7 bytes in download segment\n");
      return -EINVAL;
    }

  unused = 7 - datalen;

  memset(&msg, 0, sizeof(msg));
  msg.data[0] = CANOPEN_SDO_COMMAND_DNS;
  if (toggle)
    {
      msg.data[0] |= CANOPEN_SDO_SEG_TOGGLE;
    }
  if (cont)
    {
      msg.data[0] |= CANOPEN_SDO_SEG_CONTINUE;
    }
  msg.data[0] &= ~CANOPEN_SDO_SEG_ULEN_MASK;
  msg.data[0] |= (unused << CANOPEN_SDO_SEG_ULEN_SHIFT);
  
  //bytes 1..7 encode data
  memcpy(msg.data+1, data, datalen);
  memset(msg.data+1+datalen, 0, unused);

  msg.id  = 0x580 + tgtid;
  msg.dlc = 8;

  return canopen_master_tx(master, &msg);
}

/* -------------------------------------------------------------------------- */
int canopen_sdoclient_download(FAR struct canopen_master_s *master, int tgtid,
                               uint16_t index, uint8_t subindex, bool expedited,
                               uint8_t *data, uint32_t datalen)
{
  int ret;
  struct timespec tm;
  uint8_t cmd;
  uint32_t reason;
  uint32_t dataoff;
  bool more;
  bool toggle;
  uint16_t rxind;
  uint8_t  rxsub;

  struct canmsg_s *msg = &master->sdocli.sdoc_rsp;

  master->sdocli.sdoc_pending   = SDO_PENDING_DOWNLOAD;
  master->sdocli.sdoc_pendingid = tgtid;

  ret = canopen_sdocli_send_dli(master, tgtid, index, subindex, expedited, data, datalen);
  if(ret<0)
    {
      coerr("CAN write failed, errno=%d\n",errno);
      goto done;
    }

  //wait for a response message

wait_response:
  if (clock_gettime(CLOCK_REALTIME, &tm) < 0)
    {
      coerr("clock_gettime() failed\n");
      ret = -errno;
      goto done;
    }
  tm.tv_sec += master->sdocli.sdoc_timeout;

again:
  ret = sem_timedwait(&master->sdocli.sdoc_rxsem, &tm);
  if (ret==-1)
    {
      if (errno==ETIMEDOUT)
        {
          coerr("Timeout while waiting for sdo response from server\n");
          ret = -errno;
          goto done;
        }
      else if (errno==EINTR)
        {
          cowarn("Interrupted by signal.\n");
          goto again;
        }
      else
        {
          ret = -errno;
          goto done;
        }
    }

  coinfo("Got response\n");

  cmd = msg->data[0] & CANOPEN_SDO_COMMAND_MASK;
  switch (cmd)
    {
      case CANOPEN_SDO_COMMAND_ABORT:
        //Server cannot proceed with current command
        reason   = msg->data[7];
        reason <<= 8;
        reason  |= msg->data[6];
        reason <<= 8;
        reason  |= msg->data[5];
        reason <<= 8;
        reason  |= msg->data[4];

        coinfo("Server aborted the transfer, reason: %08X (%s)\n", reason, canopen_sdo_whyabort(reason));
        break;

      case CANOPEN_SDO_RESPONSE_DNI:
        //Response to download initiate.
        coinfo("Server Download Initiate response\n");

        //check multiplexer
        rxind   = msg->data[2];
        rxind <<= 8;
        rxind  |= msg->data[1];
        rxsub   = msg->data[3];
        if (rxind != index || rxsub != subindex)
          {
            coerr("Response multiplexer does not match request\n");
            ret = -EIO;
            break;
          }

        //If the transfer was expedited, we're done.
        if(expedited)
          {
            coinfo("Download expedited transaction complete\n");
            break;
          }
        //Else, send a download segment and receive again
        else
          {
            uint32_t todolen = datalen;
            more = (todolen > 7);
            if(more)
              {
                todolen = 7;
              }
            coinfo("Continuing Download transaction with a segment\n");
            coinfo("offset 0 todolen %d datalen %d\n", todolen, datalen);
            dataoff = 0;
            toggle  = false;
            ret = canopen_sdocli_send_dns(master, tgtid, toggle, more, data+dataoff, todolen);
            if(ret<0)
              {
                coerr("CAN write failed, errno=%d\n",ret);
                break;
              }
            dataoff += todolen;
            datalen -= todolen;
            goto wait_response;
          }
        break;

      case CANOPEN_SDO_RESPONSE_DNS:
        coinfo("Server Download Segment response\n");
        //Response to download segment
        //If this is the last block, we're done.
        if (!more)
          {
            coinfo("Download transaction complete\n");
          }
        //Else, send another download segment and try again
        else
          {
            uint32_t todolen = datalen;
            more = (todolen > 7);
            if (more)
              {
                todolen = 7;
              }
            toggle = !toggle;
            coinfo("Continuing Download transaction with another segment\n");
            coinfo("Todolen %u, datalen %u offset %d toggle %d\n", todolen, datalen, dataoff, toggle);
            ret = canopen_sdocli_send_dns(master, tgtid, toggle, more, data+dataoff, todolen);
            if(ret<0)
              {
                coerr("CAN write failed, errno=%d\n",errno);
                break;
              }
            dataoff += todolen;
            datalen -= todolen;
            goto wait_response;
          }
        break;

      default:
        ret = -EIO;

    } //switch

done:
  master->sdocli.sdoc_pending   = SDO_PENDING_NONE;
  master->sdocli.sdoc_pendingid = 0;
  return ret;
}

/* -------------------------------------------------------------------------- */
/* Send Upload initiate Command */
static int canopen_sdocli_send_upi(FAR struct canopen_master_s *master, int tgtid,
                                   uint16_t index, uint8_t subindex)
{
  struct canmsg_s msg;

  memset(&msg, 0, sizeof(msg));
  msg.data[0] = CANOPEN_SDO_COMMAND_UPI;
  msg.data[1] = (uint8_t)(index & 0xFF);
  msg.data[2] = (uint8_t)(index >> 8);
  msg.data[3] = (uint8_t) subindex;

  msg.id  = 0x580 + tgtid;
  msg.dlc = 8;

  return canopen_master_tx(master, &msg);
}

/* -------------------------------------------------------------------------- */
/* Send Upload Segment Command */
static int canopen_sdocli_send_ups(FAR struct canopen_master_s *master, int tgtid,
                                   bool toggle)
{
  struct canmsg_s msg;

  memset(&msg, 0, sizeof(msg));
  msg.data[0] = CANOPEN_SDO_COMMAND_UPS;
  if (toggle)
    {
      msg.data[0] |= CANOPEN_SDO_SEG_TOGGLE;
    }

  msg.id  = 0x580 + tgtid;
  msg.dlc = 8;

  return canopen_master_tx(master, &msg);
}

/* -------------------------------------------------------------------------- */
int canopen_sdoclient_upload(FAR struct canopen_master_s *master, int tgtid,
                             uint16_t index, uint8_t subindex,
                             canopen_upload_fragment_f callback, void *context)
{
  int ret;
  struct timespec tm;
  uint32_t reason;
  uint8_t cmd;
  bool toggle;
  bool cont;
  bool is_exp;
  uint32_t datalen;
  uint32_t unused;
  int transferlen;
  uint16_t rxind;
  uint8_t  rxsub;

  master->sdocli.sdoc_pending   = SDO_PENDING_UPLOAD;
  master->sdocli.sdoc_pendingid = tgtid;
  struct canmsg_s *msg = &master->sdocli.sdoc_rsp;

  ret = canopen_sdocli_send_upi(master, tgtid, index, subindex);
  if (ret<0)
    {
      coerr("CAN write failed, errno=%d\n",ret);
      goto done;
    }

  //wait for a response message

wait_response:
  if (clock_gettime(CLOCK_REALTIME, &tm) < 0)
    {
      coerr("clock_gettime() failed\n");
      master->sdocli.sdoc_pending = SDO_PENDING_NONE;
      ret = -errno;
      goto done;
    }
  tm.tv_sec += master->sdocli.sdoc_timeout;

again:
  ret = sem_timedwait(&master->sdocli.sdoc_rxsem, &tm);
  if (ret==-1)
    {
      if (errno==ETIMEDOUT)
        {
          coerr("Timeout while waiting for sdo response from server\n");
          master->sdocli.sdoc_pending = SDO_PENDING_NONE;
          ret = -errno;
          goto done;
        }
      else if (errno==EINTR)
        {
          cowarn("Interrupted by signal.\n");
          goto again;
        }
      else
        {
          master->sdocli.sdoc_pending = SDO_PENDING_NONE;
          ret = -errno;
          goto done;
        }
    }

  coinfo("Got response\n");

  cmd = msg->data[0] & CANOPEN_SDO_COMMAND_MASK;
  switch (cmd)
    {
      case CANOPEN_SDO_COMMAND_ABORT:
        //Server cannot proceed with current command
        reason   = msg->data[7];
        reason <<= 8;
        reason  |= msg->data[6];
        reason <<= 8;
        reason  |= msg->data[5];
        reason <<= 8;
        reason  |= msg->data[4];

        coinfo("Server aborted the transfer, reason: %08X (%s)\n", reason, canopen_sdo_whyabort(reason));
        callback(master, context,
                 index, subindex,
                 SDOCLIENT_CB_EVENT_ABORT, (void*)&reason, 4);
        break;

      case CANOPEN_SDO_RESPONSE_UPI:
        //Response to upload initiate.
        coinfo("Server Upload Initiate response\n");
        is_exp = (msg->data[0] & CANOPEN_SDO_INIT_EXPEDITED) == CANOPEN_SDO_INIT_EXPEDITED;

        //check multiplexer
        rxind   = msg->data[2];
        rxind <<= 8;
        rxind  |= msg->data[1];
        rxsub   = msg->data[3];
        if (rxind != index || rxsub != subindex)
          {
            coerr("Response multiplexer does not match request\n");
            master->sdocli.sdoc_pending = SDO_PENDING_NONE;
            ret = -EIO;
            break;
          }

        //If the transfer is expedited, we're done.
        if(is_exp)
          {
            unused = (msg->data[0] & CANOPEN_SDO_INIT_ULEN_MASK) >> CANOPEN_SDO_INIT_ULEN_SHIFT;
            datalen = 4 - unused;
            callback(master, context,
                     index, subindex,
                     SDOCLIENT_CB_EVENT_START, NULL, datalen);
            callback(master, context,
                     index, subindex,
                     SDOCLIENT_CB_EVENT_DATA, msg->data + 4, datalen);
            callback(master, context,
                     index, subindex,
                     SDOCLIENT_CB_EVENT_COMPLETE, NULL, 0);

            coinfo("Upload expedited transaction complete\n");
            master->sdocli.sdoc_pending = SDO_PENDING_NONE;
            break;
          }
        //Else, send a upload segment command and receive again
        else
          {
            //get full length of data from up init response
            datalen   = msg->data[7];
            datalen <<= 8;
            datalen  |= msg->data[6];
            datalen <<= 8;
            datalen  |= msg->data[5];
            datalen <<= 8;
            datalen  |= msg->data[4];

            callback(master, context,
                     index, subindex,
                     SDOCLIENT_CB_EVENT_START, NULL, transferlen);

            coinfo("Continuing Upload transaction with a segment (total %u)\n",datalen);
            toggle = false;
            ret = canopen_sdocli_send_ups(master, tgtid, toggle);
            if(ret<0)
              {
                coerr("CAN write failed, errno=%d\n",errno);
                break;
              }
            goto wait_response;
          }
        break;

      case CANOPEN_SDO_RESPONSE_UPS:
        toggle      = (msg->data[0] & CANOPEN_SDO_SEG_TOGGLE) == CANOPEN_SDO_SEG_TOGGLE;
        cont        = (msg->data[0] & CANOPEN_SDO_SEG_CONTINUE) == CANOPEN_SDO_SEG_CONTINUE;
        unused      = (msg->data[0] & CANOPEN_SDO_SEG_ULEN_MASK) >> CANOPEN_SDO_SEG_ULEN_SHIFT;
        transferlen = 7 - unused;

        coinfo("Upload segment response with %d bytes, toggle=%d, continue=%d\n",
               transferlen, toggle, cont);

        callback(master, context,
                 index, subindex,
                 SDOCLIENT_CB_EVENT_DATA, msg->data + 1, transferlen);

        //Response to upload segment
        //If this is the last block, we're done.
        if (!cont)
          {
            coinfo("Transaction complete\n");
            callback(master, context,
                     index, subindex,
                     SDOCLIENT_CB_EVENT_COMPLETE, NULL, 0);
            break;
          }
        //Else, send a upload segment and try again
        else
          {
            coinfo("More data to be read\n");
            toggle = !toggle;
            ret = canopen_sdocli_send_ups(master, tgtid, toggle);
            if(ret<0)
              {
                coerr("CAN write failed, errno=%d\n",ret);
                break;
              }
            goto wait_response;
          }
        break;

      default:
        ret = -EIO;

    } //switch
done:
  master->sdocli.sdoc_pending   = SDO_PENDING_NONE;
  master->sdocli.sdoc_pendingid = 0;
  return ret;
}
