/************************************************************************************
 * apps/canutils/libcanopen/canopen.h
 *
 *   Copyright (C) 2016 Sebastien Lorquet. All rights reserved.
 *   Author: Sebastien Lorquet <sebastien@lorquet.fr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ************************************************************************************/

#ifndef __APPS_CANUTILS_LIBCANOPEN_CANOPEN_H
#define __APPS_CANUTILS_LIBCANOPEN_CANOPEN_H

#include <stdint.h>

#ifdef CONFIG_DEBUG_CANOPEN_ERROR
#  define coerr(format, ...)     _err(format, ##__VA_ARGS__)
#else
#  define coerr(x...)
#endif

#ifdef CONFIG_DEBUG_CANOPEN_WARN
#  define cowarn(format, ...)   _warn(format, ##__VA_ARGS__)
#else
#  define cowarn(x...)
#endif

#ifdef CONFIG_DEBUG_CANOPEN_INFO
#  define coinfo(format, ...)   _info(format, ##__VA_ARGS__)
#else
#  define coinfo(x...)
#endif

#ifdef CONFIG_DEBUG_CANOPEN
#  define coerrdumpbuffer(m,b,n)  errdumpbuffer(m,b,n)
#  define coinfodumpbuffer(m,b,n) infodumpbuffer(m,b,n)
#else
#  define coerrdumpbuffer(m,b,n)
#  define coinfodumpbuffer(m,b,n)
#endif

struct canmsg_s
{
  uint32_t id;
  uint8_t  data[8];
  uint8_t  dlc;
};

/*Function codes, tx/rx are from device point of view */
#define CANOPEN_CANIDBASE_SDOTX 0x580
#define CANOPEN_CANIDBASE_SDORX 0x600

/* this is the type of a function used to transmit messages on a bus */
typedef int (*canopen_txcb_f)(FAR void *priv, FAR struct canmsg_s *msg);

#endif /* __APPS_CANUTILS_LIBCANOPEN_CANOPEN_OBJDIR_H */

