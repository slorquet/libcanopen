#include <stdlib.h>
#include <errno.h>
#include <debug.h>

#include "canutils/libcanopen/slave.h"
#include "canutils/libcanopen/nmt.h"

/* bracketed numbers are reference to cia-301 7.3.2.1 page 83 */

/* -------------------------------------------------------------------------- */
int canopen_nmtslave_init(FAR struct canopen_slave_s *slave)
{
  slave->nmtstate   = NMT_STATE_INITIALIZING; /* [1] */
  return OK;
}

/* -------------------------------------------------------------------------- */
static int canopen_nmtslave_resetapp(FAR struct canopen_slave_s *slave)
{
  coinfo("NMT request: RESET APPLICATION\n");
  if (slave->nmtstate == NMT_STATE_INITIALIZING)
    {
      coinfo("OK from INITIALIZING [15] (automatic)\n");
    }
  else if (slave->nmtstate == NMT_STATE_OPERATIONAL)
    {
      coinfo("OK from OPERATIONAL [9]\n");
    }
  else if (slave->nmtstate == NMT_STATE_STOPPED)
    {
      coinfo("OK from STOPPED [10]\n");
    }
  else if (slave->nmtstate == NMT_STATE_PRE_OPERATIONAL)
    {
      coinfo("OK from PRE_OPERATIONAL [11]\n");
    }
  else
    {
      coerr("Invalid state transition to RESET_APPLICATION\n");
      return -EACCES;
    }
  slave->nmtstate = NMT_STATE_RESET_APPLICATION;
  return 0;
}

/* -------------------------------------------------------------------------- */
static int canopen_nmtslave_resetcomm(FAR struct canopen_slave_s *slave)
{
  coinfo("NMT request: RESET COMMUNICATION\n");
  if (slave->nmtstate == NMT_STATE_RESET_APPLICATION)
    {
      coinfo("OK from RESET APPLICATION [16] (automatic)\n");
    }
  else if (slave->nmtstate == NMT_STATE_OPERATIONAL)
    {
      coinfo("OK from OPERATIONAL [12]\n");
    }
  else if (slave->nmtstate == NMT_STATE_STOPPED)
    {
      coinfo("OK from STOPPED [13]\n");
    }
  else if (slave->nmtstate == NMT_STATE_PRE_OPERATIONAL)
    {
      coinfo("OK from PRE_OPERATIONAL [14]\n");
    }
  else
    {
      coerr("Invalid state transition to RESET_COMMUNICATION\n");
      return -EACCES;
    }
  slave->nmtstate = NMT_STATE_RESET_COMMUNICATION;
  return 0;
}

/* -------------------------------------------------------------------------- */
static int canopen_nmtslave_preop(FAR struct canopen_slave_s *slave)
{
  coinfo("NMT request: ENTER PRE-OPERATIONAL\n");
  if (slave->nmtstate == NMT_STATE_RESET_COMMUNICATION)
    {
      coinfo("OK from RESET_COMM [2] (automatic)\n");
    }
  else if (slave->nmtstate == NMT_STATE_OPERATIONAL)
    {
      coinfo("OK from OPERATIONAL [4]\n");
    }
  else if (slave->nmtstate == NMT_STATE_STOPPED)
    {
      coinfo("OK from STOPPED [7]\n");
    }
  else
    {
      coerr("Invalid state transition to PRE_OPERATIONAL\n");
      return -EACCES;
    }
  slave->nmtstate = NMT_STATE_PRE_OPERATIONAL;
  return OK;
}

/* -------------------------------------------------------------------------- */
static int canopen_nmtslave_start(FAR struct canopen_slave_s *slave)
{
  coinfo("NMT request: START\n");
  if (slave->nmtstate == NMT_STATE_PRE_OPERATIONAL)
    {
      coinfo("OK from PRE_OPERATIONAL [3]\n");
    }
  else if (slave->nmtstate == NMT_STATE_STOPPED)
    {
      coinfo("OK from STOPPED [6]\n");
    }
  else
    {
      coerr("Invalid state transition to OPERATIONAL\n");
      return -EACCES;
    }
  slave->nmtstate = NMT_STATE_OPERATIONAL;
  return 0;
}

/* -------------------------------------------------------------------------- */
static int canopen_nmtslave_stop(FAR struct canopen_slave_s *slave)
{
  coinfo("NMT request: STOP\n");
  if (slave->nmtstate == NMT_STATE_PRE_OPERATIONAL)
    {
      coinfo("OK from PRE_OPERATIONAL [5]\n");
    }
  else if (slave->nmtstate == NMT_STATE_OPERATIONAL)
    {
      coinfo("OK from OPERATIONAL [8]\n");
    }
  else
    {
      coerr("Invalid state transition to STOPPED\n");
      return -EACCES;
    }
  slave->nmtstate = NMT_STATE_STOPPED;
  return 0;
}

/* -------------------------------------------------------------------------- */
int canopen_nmtslave_setstate(FAR struct canopen_slave_s *slave, uint8_t deststate)
{
  int ret;

  switch(deststate)
    {
      case NMT_STATE_RESET_APPLICATION:
        ret = canopen_nmtslave_resetapp(slave);
        if(ret<0)
          {
            return ret;
          }
        //Automatic transition to reset comm 
      case NMT_STATE_RESET_COMMUNICATION:
        ret = canopen_nmtslave_resetcomm(slave);
        if(ret<0)
          {
            return ret;
          }
        //Automatic transition to pre-op
      case NMT_STATE_PRE_OPERATIONAL:
        return canopen_nmtslave_preop(slave);

      case NMT_STATE_OPERATIONAL:
        return canopen_nmtslave_start(slave);

      case NMT_STATE_STOPPED:
        return canopen_nmtslave_stop(slave);
    }

  return -EINVAL;
}

/* -------------------------------------------------------------------------- */
/* Manage nmt state transitions :
 - can-id 0 (start/stop/pre-op/resets)
*/
int canopen_nmtslave_rxcan(FAR struct canopen_slave_s *slave, FAR struct canmsg_s *msg)
{
  uint8_t deststate;

  if (msg->id != 0)
    {
    coerr("NMT message: Unexpected msg id %d\n", msg->id);
    return 0;
    }

  if (msg->dlc != 2)
    {
    coerr("NMT message: Incorrect length %d\n",msg->dlc);
    return 0;
    }

  if (msg->data[1] != 0)
    {
      if (msg->data[1] != slave->id)
        {
          coinfo("NMT message for someone else\n");
          return 0;
        }
    }

  /* the message is for us */
  switch(msg->data[0])
    {
      case NMT_CMD_START     : deststate = NMT_STATE_OPERATIONAL;         break;
      case NMT_CMD_STOP      : deststate = NMT_STATE_STOPPED;             break;
      case NMT_CMD_PREOP     : deststate = NMT_STATE_PRE_OPERATIONAL;     break;
      case NMT_CMD_RESET_APP : deststate = NMT_STATE_RESET_APPLICATION;   break;
      case NMT_CMD_RESET_COMM: deststate = NMT_STATE_RESET_COMMUNICATION; break;
      default:
        coerr("Invalid NMT message %d\n", msg->data[0]);
        return -EINVAL;
    }
  return canopen_nmtslave_setstate(slave, deststate);
}

/* -------------------------------------------------------------------------- */
int canopen_nmtslave_getstate(FAR struct canopen_slave_s *slave, FAR uint8_t *state)
{
  *state = slave->nmtstate;
  return OK;
}

