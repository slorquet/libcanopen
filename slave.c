/* This is a CANopen slave node */

#include "nuttx/config.h"
#include <stdio.h>
#include <debug.h>
#include <errno.h>

#include "canutils/libcanopen/canopen.h"
#include "canutils/libcanopen/slave.h"
#include "canutils/libcanopen/objdir.h"
#include "canutils/libcanopen/sdo.h"
#include "canutils/libcanopen/nmt.h"

#define DT_OSDEBUG   0x0024
#define DT_OSCOMMAND 0x0025

const struct canopen_var_s canopen_dcp[] = 
{
  /* as per CIA-301 */

/* Index         Sub   Code      Type            Access  Info */
  {DT_OSDEBUG  , 0x00, OC_DEFSTRUCT, DT_UNSIGNED8 , 0     , 3}, /* OS debug record, see page 94 */
  {DT_OSDEBUG  , 0x01, OC_DEFSTRUCT, DT_OCTSTRING , AA_WO , 0}, /* Command */
  {DT_OSDEBUG  , 0x02, OC_DEFSTRUCT, DT_UNSIGNED8 , AA_RO , 0}, /* Status */
  {DT_OSDEBUG  , 0x03, OC_DEFSTRUCT, DT_OCTSTRING , AA_RO , 0}, /* Reply */

  {DT_OSCOMMAND, 0x00, OC_DEFSTRUCT, DT_UNSIGNED8 , 0     , 3}, /* OS command record, see page 94 */
  {DT_OSCOMMAND, 0x01, OC_DEFSTRUCT, DT_OCTSTRING , AA_WO , 0}, /* Command */
  {DT_OSCOMMAND, 0x02, OC_DEFSTRUCT, DT_UNSIGNED8 , AA_RO , 0}, /* Status */
  {DT_OSCOMMAND, 0x03, OC_DEFSTRUCT, DT_OCTSTRING , AA_RO , 0}, /* Reply */

/* Index   Sub   Code      Type            Access  Info */
  {0x1000, 0x00, OC_VAR   , DT_UNSIGNED32, AA_RO   ,    0}, /* Device type */
  {0x1001, 0x00, OC_VAR   , DT_UNSIGNED8 , AA_RO   ,    0}, /* Error register */
  {0x1002, 0x00, OC_VAR   , DT_UNSIGNED32, AA_RO   ,    0}, /* Manufacturer error register */

#if 0
  {0x1003, 0x00, OC_ARRAY , DT_UNSIGNED32, 0       , 0xFE}, /* Pre-defined error field */
  {0x1003, 0x01, OC_ARRAY , DT_UNSIGNED32, AA_RO   , 0xFE}, /* Standard error field entries */
#else
  {0x1003, 0x00, OC_ARRAY , DT_UNSIGNED32, 0       , 16}, /* Pre-defined error field */
  {0x1003, 0x01, OC_ARRAY , DT_UNSIGNED32, AA_RO   , 16}, /* Standard error field entries */
#endif

  /* 0x1004 Reserved for compatibility reasons */
  {0x1005, 0x00, OC_VAR   , DT_UNSIGNED32, AA_RW   ,    0}, /* COB-ID SYNC */
  {0x1006, 0x00, OC_VAR   , DT_UNSIGNED32, AA_RW   ,    0}, /* communication cycle period */
  {0x1007, 0x00, OC_VAR   , DT_UNSIGNED32, AA_RW   ,    0}, /* synchronous window length */
  {0x1008, 0x00, OC_VAR   , DT_VISSTRING , AA_CONST,    0}, /* manufacturer device name */
  {0x1009, 0x00, OC_VAR   , DT_VISSTRING , AA_CONST,    0}, /* manufacturer hardware version */
  {0x100A, 0x00, OC_VAR   , DT_VISSTRING , AA_CONST,    0}, /* manufacturer software version */
  /* 0x100B Reserved for compatibility reasons */
  {0x100C, 0x00, OC_VAR   , DT_UNSIGNED16, AA_RW   ,    0}, /* guard time */
  {0x100D, 0x00, OC_VAR   , DT_UNSIGNED8 , AA_RW   ,    0}, /* life time factor */
  /* 0x100E Reserved for compatibility reasons */
  /* 0x100F Reserved for compatibility reasons */

  {0x1010, 0x00, OC_ARRAY , DT_UNSIGNED32, 0       ,    4}, /* save parameters */
  {0x1010, 0x01, OC_ARRAY , DT_UNSIGNED32, AA_RW   ,    0}, /* save all params */
  {0x1010, 0x02, OC_ARRAY , DT_UNSIGNED32, AA_RW   ,    0}, /* save communication params */
  {0x1010, 0x03, OC_ARRAY , DT_UNSIGNED32, AA_RW   ,    0}, /* save application params */
  {0x1010, 0x04, OC_ARRAY , DT_UNSIGNED32, AA_RW   ,    0}, /* save manufacturer defined params */

  {0x1011, 0x00, OC_ARRAY , DT_UNSIGNED32, 0       ,    4}, /* restore default parameters */
  {0x1011, 0x01, OC_ARRAY , DT_UNSIGNED32, AA_RW   ,    0}, /* restore all default params */
  {0x1011, 0x02, OC_ARRAY , DT_UNSIGNED32, AA_RW   ,    0}, /* restore default communication params */
  {0x1011, 0x03, OC_ARRAY , DT_UNSIGNED32, AA_RW   ,    0}, /* restore default application params */
  {0x1011, 0x04, OC_ARRAY , DT_UNSIGNED32, AA_RW   ,    0}, /* restore default manufacturer defined params */

  {0x1012, 0x00, OC_VAR   , DT_UNSIGNED32, AA_RW   ,    0}, /* COB-ID Timestamp */
  {0x1013, 0x00, OC_VAR   , DT_UNSIGNED32, AA_RW   ,    0}, /* High Resolution Timestamp */
  {0x1014, 0x00, OC_VAR   , DT_UNSIGNED32, AA_RW   ,    0}, /* COB-ID EMCY */
  {0x1015, 0x00, OC_VAR   , DT_UNSIGNED16, AA_RW   ,    0}, /* Inhibit time EMCY */
#if 0
  {0x1016, 0x00, OC_ARRAY , DT_UNSIGNED32, 0       ,  127}, /* Consumer Heart Beat Time */
  {0x1016, 0x01, OC_ARRAY , DT_UNSIGNED32, AA_RW   ,  127}, /* Consumer Heart Beat Time */

  {0x1017, 0x00, OC_VAR   , DT_UNSIGNED16, 0       ,    4}, /* Producer Heart Beat Time */
#endif

  {0x1018, 0x00, OC_RECORD, DT_IDENTITY  , 0       ,    0}, /* Identity object - 4 fields */

  {0x1019, 0x00, OC_VAR   , DT_UNSIGNED8 , AA_RW   ,    0}, /* Synchronous counter overflow value */

  {0x1020, 0x00, OC_ARRAY , DT_UNSIGNED32, 0       ,    2}, /* Verify configuration */
  {0x1020, 0x01, OC_ARRAY , DT_UNSIGNED32, AA_RW   ,    0}, /* Configuration date */
  {0x1020, 0x02, OC_ARRAY , DT_UNSIGNED32, AA_RW   ,    0}, /* Configuration time */

  {0x1021, 0x00, OC_VAR   , DT_DOMAIN    , AA_RO   ,    0}, /* Store Electronic Datasheet */
  {0x1022, 0x00, OC_VAR   , DT_UNSIGNED16, AA_RO   ,    0}, /* Store format */

  {0x1023, 0x00, OC_RECORD, DT_OSCOMMAND , 0       ,    0}, /* OS command */

  {0x1024, 0x00, OC_VAR   , DT_UNSIGNED8 , AA_WO   ,    0}, /* OS command mode */

  {0x1025, 0x00, OC_RECORD, DT_OSDEBUG   , AA_RW   ,    0}, /* OS debugger interface */

  {0x1026, 0x00, OC_ARRAY , DT_UNSIGNED8 , 0       ,    3}, /* OS prompt */
  {0x1026, 0x01, OC_ARRAY , DT_UNSIGNED8 , AA_WO   ,    0}, /* stdin */
  {0x1026, 0x02, OC_ARRAY , DT_UNSIGNED8 , AA_RO   ,    0}, /* stdout */
  {0x1026, 0x03, OC_ARRAY , DT_UNSIGNED8 , AA_RO   ,    0}, /* stderr */

//from cia 302
  {0x1F50, 0x00, OC_ARRAY , DT_DOMAIN    , 0       ,    1}, /* Program download */
  {0x1F50, 0x01, OC_ARRAY , DT_DOMAIN    , AA_RW   ,    0}, /* Program number 1 (firmware image) */
};

#define CANOPEN_DCP_COUNT (sizeof(canopen_dcp)/sizeof(canopen_dcp[0]))

/****************************************************************************
 * canopen_slave_varupdated
 * Description:
 *   Called when a variable in the object directory has been updated.
 ****************************************************************************/
static int canopen_slave_varupdated(FAR struct canopen_slave_s *slave, FAR void *ctx,
                             uint16_t index, uint8_t subindex,
                             FAR uint8_t *data, uint32_t datalen)
{
  coinfo("var index 0x%04X sub 0x%02X updated with %u bytes at offset %u\n", index, subindex, datalen);
  return OK;
}

/****************************************************************************
 * canopen_slave_init
 * Description:
 *   Initialize the CANopen node.
 ****************************************************************************/
int canopen_slave_init(FAR struct canopen_slave_s *slave, int nodeid, canopen_txcb_f txcb, FAR void *txpriv)
{
  int ret;
  coinfo("Node init\n");
  if(nodeid<1 || nodeid>127)
    {
      return -EINVAL;
    }

  slave->id      = nodeid;
  slave->tx      = txcb;
  slave->tx_priv = txpriv;

  /* Init the NMT slave state machine */
  ret = canopen_nmtslave_init(slave);
  if(ret != OK)
    {
      coerr("Failed to initialize the NMT layer\n");
      return ret;
    }

  /* Prepare the object directory */
  ret = canopen_objdir_init(&slave->objdir);
  if(ret != OK)
    {
      coerr("Failed to initialize object directory\n");
      return ret;
    }

  /* register the default communication profile */
  ret = canopen_objdir_add(&slave->objdir, canopen_dcp, CANOPEN_DCP_COUNT);
  if(ret != OK)
    {
      coerr("Failed to add DCP to the object directory\n");
      return ret;
    }

  /* TODO set constants */

  /* Allocate memory for the object dir, now no more vars can be defined */
  ret = canopen_objdir_setup(&slave->objdir);
  if(ret != OK)
    {
      coerr("Failed to setup the object directory\n");
      return ret;
    }

  /* Run automatic state sequence to pre-operational */
  ret = canopen_nmtslave_setstate(slave, NMT_STATE_RESET_APPLICATION);
  if(ret != OK)
    {
      coerr("Failed to start the NMT layer\n");
      return ret;
    }

  /* Prepare the sdo server */
  ret = canopen_sdoserver_init(&slave->sdoserver, canopen_slave_varupdated);
  if(ret != OK)
    {
      coerr("Failed to initialize SDO server\n");
      return ret;
    }

  return OK;
}

/****************************************************************************
 * canopen_slave_stop
 * Description:
 *   Stop the CANopen node.
 ****************************************************************************/
int canopen_slave_stop(FAR struct canopen_slave_s *slave)
{
  coinfo("Slave Node stop\n");
  return canopen_objdir_free(&slave->objdir);
}

/****************************************************************************
 * canopen_tx
 * Description:
 *   Main CAN message emitter, using user callback
 ****************************************************************************/
int canopen_slave_tx(FAR struct canopen_slave_s *slave, FAR struct canmsg_s *msg)
{
  return slave->tx(slave->tx_priv, msg);
}

/****************************************************************************
 * canopen_rxcan
 * Description:
 *   Main CAN message dispatcher
 ****************************************************************************/
int canopen_slave_rx(FAR struct canopen_slave_s *slave, FAR struct canmsg_s *msg)
{
  int id = msg->id;
  char hex[2*8+1];
  int i;
  for(i=0;i<msg->dlc;i++)
    {
      sprintf(hex+(i<<1), "%02X", msg->data[i]);
    }

  if (id == 0) /* NMT */
    {
      coinfo("NMT M>S [%3X] L=%d %s\n", id, i, hex);
      return canopen_nmtslave_rxcan(slave, msg);
    }
  /* 0x080    sync */
  /* 0x080+id emcy */
  /* 0x100    time */
  /* 0x180+id PDO1 (tx) */
  /* 0x280+id PDO2 */
  /* 0x380+id PDO3 */
  /* 0x480+id PDO4 */
  else if(id == CANOPEN_CANIDBASE_SDOTX + slave->id) /* SDO TX */
    {
      coinfo("SDO M>S [%3X] L=%d %s\n", id, i, hex);
      return canopen_sdoserver_rxcan(slave, msg);
    }
  /* 0x700+id node guarding/heartbeat/bootup */
  else
    {
      coinfo("???     [%3X] L=%d %s\n", id, i, hex);
    }
  return OK;
}

