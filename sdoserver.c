#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <nuttx/config.h>
#include <debug.h>
#include <errno.h>
#include <semaphore.h>

#include "canutils/libcanopen/slave.h"
#include "canutils/libcanopen/objdir.h"
#include "canutils/libcanopen/sdo.h"
#include "canutils/libcanopen/sdoserver.h"

/* -------------------------------------------------------------------------- */
int canopen_sdoserver_init(FAR struct canopen_sdoserver_s *server, CODE canopen_sdoserver_varupdated_f cb)
{
  memset(server, 0, sizeof(struct canopen_sdoserver_s));
  server->callback = cb;
  return OK;
}

/* -------------------------------------------------------------------------- */
/* Send a SDO abort message, can be used on server or client*/
static int canopen_sdoserver_sendabort(FAR struct canopen_slave_s *slave, uint16_t index, 
                                 uint8_t subindex, uint32_t code)
{
  struct canmsg_s msg;
  msg.data[0] = CANOPEN_SDO_COMMAND_ABORT;
  msg.data[1] = (uint8_t)( index        & 0xFF);
  msg.data[2] = (uint8_t)((index >>  8) & 0xFF);
  msg.data[3] = subindex;
  msg.data[4] = (uint8_t)( code        & 0xFF);
  msg.data[5] = (uint8_t)((code >>  8) & 0xFF);
  msg.data[6] = (uint8_t)((code >> 16) & 0xFF);
  msg.data[7] = (uint8_t)((code >> 24) & 0xFF);
  msg.dlc = 8;
  msg.id  = CANOPEN_CANIDBASE_SDORX + slave->id;
  return canopen_slave_tx(slave, &msg);
}

/* -------------------------------------------------------------------------- */
/* Send a SDO DOWNLOAD INITIATE RESPONSE */
static int canopen_sdoserver_sendrspdi(FAR struct canopen_slave_s *slave,
                                 uint16_t index, uint8_t subindex)
{
  struct canmsg_s msg;
  msg.data[0] = CANOPEN_SDO_RESPONSE_DNI;
  msg.data[1] = (uint8_t)( index        & 0xFF);
  msg.data[2] = (uint8_t)((index >>  8) & 0xFF);
  msg.data[3] = subindex;
  msg.data[4] = 0;
  msg.data[5] = 0;
  msg.data[6] = 0;
  msg.data[7] = 0;
  msg.dlc = 8;
  msg.id  = CANOPEN_CANIDBASE_SDORX + slave->id;
  return canopen_slave_tx(slave, &msg);
}

/* -------------------------------------------------------------------------- */
/* Send a SDO DOWNLOAD SEGMENT RESPONSE */
int canopen_sdoserver_sendrspds(FAR struct canopen_slave_s *slave, bool toggle)
{
  struct canmsg_s msg;

  msg.data[0] = CANOPEN_SDO_RESPONSE_DNS;

  if (toggle)
    {
      msg.data[0] |= CANOPEN_SDO_SEG_TOGGLE;
    }

  memset(msg.data+1, 0, 7);
  msg.dlc = 8;
  msg.id  = CANOPEN_CANIDBASE_SDORX + slave->id;
  return canopen_slave_tx(slave, &msg);
}

/* -------------------------------------------------------------------------- */
/* Send a SDO UPLOAD INITIATE RESPONSE */
static int canopen_sdoserver_sendrspui(FAR struct canopen_slave_s *slave,
                                 uint16_t index, uint8_t subindex,
                                 uint32_t datalen, FAR uint8_t *expdata)
{
  struct canmsg_s msg;

  if (datalen == 0)
    {
      return -EINVAL;
    }

  msg.data[0] = CANOPEN_SDO_RESPONSE_UPI | CANOPEN_SDO_INIT_SIZEIND;
  msg.data[1] = (uint8_t)( index        & 0xFF);
  msg.data[2] = (uint8_t)((index >>  8) & 0xFF);
  msg.data[3] = subindex;

  if (expdata != NULL)
    {
      /* Create an expedited transfer */
      int unused;

      if (datalen > 4)
        {
          coerr("sdo up init rsp: bad expedited transfer len\n");
          return -EINVAL;
        }
      unused = (4 - datalen) & 0x03;

      msg.data[0] |= CANOPEN_SDO_INIT_EXPEDITED;
      msg.data[0] &= ~CANOPEN_SDO_INIT_ULEN_MASK;
      msg.data[0] |= (unused << CANOPEN_SDO_INIT_ULEN_SHIFT);

      /* Store tx data in message */
      memcpy(msg.data+4, expdata, datalen);
      memset(msg.data+4+datalen , 0, unused);
    }
  else
    {
      /* Create a normal transfer */
      msg.data[0] &= ~CANOPEN_SDO_INIT_EXPEDITED;
      msg.data[0] &= ~CANOPEN_SDO_INIT_ULEN_MASK;

      /* Store tx data LEN in message */
      msg.data[4] =  datalen      & 0xFF;
      msg.data[5] = (datalen>> 8) & 0xFF;
      msg.data[6] = (datalen>>16) & 0xFF;
      msg.data[7] = (datalen>>24) & 0xFF;
    }

  msg.dlc = 8;
  msg.id  = CANOPEN_CANIDBASE_SDORX + slave->id;
  return canopen_slave_tx(slave, &msg);
}

/* -------------------------------------------------------------------------- */
/* Send a SDO UPLOAD SEGMENT RESPONSE */
int canopen_sdoserver_sendrspus(FAR struct canopen_slave_s *slave, bool toggle, bool cont, FAR uint8_t *data, uint32_t len)
{
  struct canmsg_s msg;
  int unused;

  msg.data[0] = CANOPEN_SDO_RESPONSE_UPS;

  if (toggle)
    {
      msg.data[0] |= CANOPEN_SDO_SEG_TOGGLE;
    }

  if (cont)
    {
      msg.data[0] |= CANOPEN_SDO_SEG_CONTINUE;
    }

  if (len>7)
    {
      canerr("upload segment tried to send more than 7 bytes!\n");
      return -EINVAL;
    }

  unused = 7 - len;
  msg.data[0] &= ~CANOPEN_SDO_SEG_ULEN_MASK;
  msg.data[0] |= unused << CANOPEN_SDO_SEG_ULEN_SHIFT;

  memset(msg.data+1, 0, 7);
  memcpy(msg.data+1, data, len);
  msg.dlc = 8;
  msg.id  = CANOPEN_CANIDBASE_SDORX + slave->id;
  return canopen_slave_tx(slave, &msg);
}

/* -------------------------------------------------------------------------- */
/* Download initiate: Start receiving data from the user */
static int sdoserver_manage_dni(FAR struct canopen_slave_s *slave, FAR struct canmsg_s *msg)
{
  FAR uint8_t *varloc;
  uint32_t varsz;
  uint16_t vartype;
  uint8_t  varaccess;
  uint32_t transferlen;
  int ret;
  uint16_t index    = (msg->data[2] << 8) | msg->data[1];
  uint8_t  subindex = msg->data[3];

  coinfo("Download initiate, index %04X subindex %02X\n", index, subindex);

  if (!(msg->data[0] & CANOPEN_SDO_INIT_SIZEIND))
    {
      return canopen_sdoserver_sendabort(slave, index, subindex, CANOPEN_SDOABORT_INVALID_COMMAND);
    }

  /* Get the variable */

  ret = canopen_objdir_findvar(&slave->objdir, 
                               index, subindex, &varloc, &varsz, 
                               &vartype, &varaccess);
  if (ret!=OK)
    {
      return canopen_sdoserver_sendabort(slave, index, subindex, CANOPEN_SDOABORT_NO_OBJECT);
    }

  /* Check that the variable can be written. It must be an exact match, except for strings and domains. */

  if (!(varaccess & AA_WRITE))
    {
      return canopen_sdoserver_sendabort(slave, index, subindex, CANOPEN_SDOABORT_EACCESS_RO);
    }

  /* Get transfer length */

  if (msg->data[0] & CANOPEN_SDO_INIT_EXPEDITED)
    {
      uint32_t unused   = (msg->data[0] & CANOPEN_SDO_INIT_ULEN_MASK) >> CANOPEN_SDO_INIT_ULEN_SHIFT;
      transferlen = 4 - unused;
    }
  else
    {
      transferlen   = msg->data[7];
      transferlen <<= 8;
      transferlen  |= msg->data[6];
      transferlen <<= 8;
      transferlen  |= msg->data[5];
      transferlen <<= 8;
      transferlen  |= msg->data[4];
    }

  coinfo("Download initiate: Transfer len=%d\n", transferlen);

  if (vartype == DT_DOMAIN || vartype == DT_VISSTRING || vartype == DT_OCTSTRING || vartype == DT_UNISTRING)
    {
      FAR struct canopen_objdir_dereftype_s *deref;
      coinfo("var has a variable length");

      /* check that variable can still be stored */
      if (transferlen > 256)
        {
          /* For the moment this is rejected, we should allocate a file */
          return canopen_sdoserver_sendabort(slave, index, subindex, CANOPEN_SDOABORT_BAD_LENGTH_LONG);
        }

      /* allocate a chunk of correct size */
      deref = (FAR struct canopen_objdir_dereftype_s *)varloc;
      deref->data  = realloc(deref->data, transferlen);
      deref->length = transferlen;

      /* use it */
      varloc = deref->data;
      varsz  = transferlen;
    }
  else
    {
      /* if the variable has a fixed length, check that the transfer length
       * matches the stored var length */
      if (transferlen < varsz)
        {
          return canopen_sdoserver_sendabort(slave, index, subindex, CANOPEN_SDOABORT_BAD_LENGTH_SHORT);
        }
      else if (transferlen > varsz)
        {
          return canopen_sdoserver_sendabort(slave, index, subindex, CANOPEN_SDOABORT_BAD_LENGTH_LONG);
        }
    }
  /* If transfer is expedited, store the received data in the variable */

  if (msg->data[0] & CANOPEN_SDO_INIT_EXPEDITED)
    {
      coinfo("Expedited transfer, data %02X%02X%02X%02X\n",
             msg->data[4], msg->data[5], msg->data[6], msg->data[7]);

      memcpy(varloc, msg->data+4, transferlen);
      slave->sdoserver.callback(slave, slave->sdoserver.cb_ctx, index, subindex, varloc, transferlen);

      /* When expedited data has been downloaded, there is no pending transfer. */

      slave->sdoserver.pending = SDO_PENDING_NONE;
    }
  else
    {
      /* Not an expedited transfer, remember transfer len for subsequent commands and checks */
      slave->sdoserver.pending = SDO_PENDING_DOWNLOAD;
      slave->sdoserver.pending_index = index;
      slave->sdoserver.pending_subindex = subindex;
      slave->sdoserver.varloc  = varloc;
      slave->sdoserver.totlen  = transferlen;
      slave->sdoserver.curoff  = 0;
      slave->sdoserver.expected_toggle = false;
    }

  /* We are done, send response */

  return canopen_sdoserver_sendrspdi(slave, index, subindex);
}

/* -------------------------------------------------------------------------- */
/* Download segment:  Receive some data from the user */
static int sdoserver_manage_dns(FAR struct canopen_slave_s *slave, FAR struct canmsg_s *msg)
{
  uint16_t index;
  uint8_t  subindex;
  uint32_t transferlen;
  bool     toggle      = (msg->data[0] & CANOPEN_SDO_SEG_TOGGLE) == CANOPEN_SDO_SEG_TOGGLE;
  bool     cont        = (msg->data[0] & CANOPEN_SDO_SEG_CONTINUE) == CANOPEN_SDO_SEG_CONTINUE;
  uint32_t unused      = (msg->data[0] & CANOPEN_SDO_SEG_ULEN_MASK) >> CANOPEN_SDO_SEG_ULEN_SHIFT;

  transferlen = 7 - unused;
  coinfo("Download segment with %d bytes, toggle=%d, continue=%d total=%u offset=%u\n",
         transferlen, toggle, cont, slave->sdoserver.totlen, slave->sdoserver.curoff);

  /* Check that a download transfer is in progress */

  if (slave->sdoserver.pending != SDO_PENDING_DOWNLOAD)
    {
      slave->sdoserver.pending = SDO_PENDING_NONE;
      return canopen_sdoserver_sendabort(slave, 0, 0, CANOPEN_SDOABORT_NO_TOGGLE);
    }

  /* get info about pending transfer */
  index    = slave->sdoserver.pending_index;
  subindex = slave->sdoserver.pending_subindex;

  /* Check correct state of toggle */

  if (toggle != slave->sdoserver.expected_toggle)
    {
      slave->sdoserver.pending = SDO_PENDING_NONE;
      return canopen_sdoserver_sendabort(slave, index, subindex, CANOPEN_SDOABORT_INVALID_COMMAND);
    }
  slave->sdoserver.expected_toggle = !slave->sdoserver.expected_toggle;

  /* Store the received data */

  memcpy(slave->sdoserver.varloc + slave->sdoserver.curoff, msg->data + 1, transferlen);
  slave->sdoserver.curoff += transferlen;

  /* Notify the user if that was the last segment (transaction is complete) */

  if (!cont)
    {
      coinfo("Download transaction is complete.\n");
      slave->sdoserver.pending = SDO_PENDING_NONE;
      if(slave->sdoserver.curoff < slave->sdoserver.totlen)
        {
          cowarn("WARNING all expected data was not transferred!\n");
        }
      if (slave->sdoserver.callback)
        {
          slave->sdoserver.callback(slave, slave->sdoserver.cb_ctx, index, subindex, 
                                    slave->sdoserver.varloc, slave->sdoserver.totlen);
        }
    }

  /* Build the response */
  return canopen_sdoserver_sendrspds(slave, toggle);
}

/* -------------------------------------------------------------------------- */
/* Upload initiate:   Start sending data to the user*/
static int sdoserver_manage_upi(FAR struct canopen_slave_s *slave, FAR struct canmsg_s *msg)
{
  FAR uint8_t *varloc;
  uint32_t varsz;
  uint16_t vartype;
  uint8_t  varaccess;
  int      ret;
  uint16_t index    = (msg->data[2] << 8) | msg->data[1];
  uint8_t  subindex = msg->data[3];

  coinfo("Upload initiate, index %04X subindex %02X\n", index, subindex);

  /* Get the variable */

  ret = canopen_objdir_findvar(&(slave->objdir), 
                               index, subindex, &varloc, &varsz, 
                               &vartype, &varaccess);
  if (ret != OK)
    {
      coerr("SDO abort: Requested var not found\n");
      return canopen_sdoserver_sendabort(slave, index, subindex, CANOPEN_SDOABORT_NO_OBJECT);
    }

  /* Check that the variable can be read. */

  if (!(varaccess & AA_READ))
    {
      coerr("SDO abort: var not readable\n");
      return canopen_sdoserver_sendabort(slave, index, subindex, CANOPEN_SDOABORT_EACCESS_WO);
    }

  /* For variable length objects, dereference object memory */

  if (vartype == DT_DOMAIN || vartype == DT_VISSTRING || vartype == DT_OCTSTRING || vartype == DT_UNISTRING)
    {
      FAR struct canopen_objdir_dereftype_s *deref;
      coinfo("var has a variable length, must dereference\n");
      deref = (FAR struct canopen_objdir_dereftype_s *)varloc;
      varloc = deref->data;
      varsz  = deref->length;

      if (varsz==0xFFFFFFFF)
        {
          coinfo("Variable-length data references file: %s\n", (char*)varloc);
        }
    }

  if (varsz > 4)
    {
      /* For transfers longer than 4 bytes, store info about current transfer */
      coinfo("sdo upload init response, not expedited\n");
      slave->sdoserver.pending = SDO_PENDING_UPLOAD;
      slave->sdoserver.varloc  = varloc;
      slave->sdoserver.totlen  = varsz;
      slave->sdoserver.curoff  = 0;
      slave->sdoserver.expected_toggle = false;
      varloc = NULL; // No data in response message for non-expedited transfer
    }
  else
    {
      /* For transfers shorter than 4 bytes, create an expedited transfer. */
      coinfo("sdo upload init response, expedited (len %d)\n", varsz);
      // when expedited data will be uploaded, there is no pending transfer.
      slave->sdoserver.pending = SDO_PENDING_NONE;
    }

  return canopen_sdoserver_sendrspui(slave, index, subindex, varsz, varloc);
}

/* -------------------------------------------------------------------------- */
/* Upload segment:    Return some data to the user */
static int sdoserver_manage_ups(FAR struct canopen_slave_s *slave, FAR struct canmsg_s *msg)
{
  uint16_t index;
  uint8_t  subindex;
  uint32_t curlen;
  bool cont;
  bool toggle = (msg->data[0] & CANOPEN_SDO_SEG_TOGGLE) == CANOPEN_SDO_SEG_TOGGLE;

  coinfo("Upload segment, toggle=%d\n", toggle);

  /* Check that an upload transfer is in progress */

  if (slave->sdoserver.pending != SDO_PENDING_UPLOAD)
    {
      slave->sdoserver.pending = SDO_PENDING_NONE;
      return canopen_sdoserver_sendabort(slave, 0, 0, CANOPEN_SDOABORT_INVALID_COMMAND);
    }
  index    = slave->sdoserver.pending_index;
  subindex = slave->sdoserver.pending_subindex;

  /* Check correct state of toggle */

  if (toggle != slave->sdoserver.expected_toggle)
    {
      slave->sdoserver.pending = SDO_PENDING_NONE;
      return canopen_sdoserver_sendabort(slave, index, subindex, CANOPEN_SDOABORT_NO_TOGGLE);
    }
  slave->sdoserver.expected_toggle = !slave->sdoserver.expected_toggle;
  curlen = slave->sdoserver.totlen - slave->sdoserver.curoff;
  cont   = curlen > 7;
  curlen = cont ? 7 : curlen;
  coinfo("pending len %u tot %u off %u cont %d willdo %d\n",
         (slave->sdoserver.totlen - slave->sdoserver.curoff), slave->sdoserver.totlen, slave->sdoserver.curoff, cont, curlen);

  slave->sdoserver.curoff += curlen;

  if(!cont)
    {
      //these were the last bytes, transfer is done
      slave->sdoserver.pending = SDO_PENDING_NONE;
      coinfo("Upload transaction complete.\n");
    }

  return canopen_sdoserver_sendrspus(slave, toggle, 
                                     cont,
                                     slave->sdoserver.varloc+slave->sdoserver.curoff,
                                     curlen);
}

/* -------------------------------------------------------------------------- */
/* manage incoming SDO server commands */
int canopen_sdoserver_rxcan(FAR struct canopen_slave_s *slave, FAR struct canmsg_s *msg)
{

  //message must have 8 bytes
  if(msg->dlc != 8)
    {
      return canopen_sdoserver_sendabort(slave, 0, 0, CANOPEN_SDOABORT_INVALID_COMMAND);
    }

  //get the command
  switch (msg->data[0] & CANOPEN_SDO_COMMAND_MASK)
    {
      case CANOPEN_SDO_COMMAND_DNI: return sdoserver_manage_dni(slave, msg); break;
      case CANOPEN_SDO_COMMAND_DNS: return sdoserver_manage_dns(slave, msg); break;
      case CANOPEN_SDO_COMMAND_UPI: return sdoserver_manage_upi(slave, msg); break;
      case CANOPEN_SDO_COMMAND_UPS: return sdoserver_manage_ups(slave, msg); break;

      case CANOPEN_SDO_COMMAND_ABORT: /* Abort transfer (client command) */
        coinfo("Client aborted current transfer\n");
        slave->sdoserver.pending = SDO_PENDING_NONE;
        /* The server does not respond */
        return OK;
        break;

      default:
        coerr("Unknown SDO Command, data0=0x%02X\n",msg->data[0]);
        return canopen_sdoserver_sendabort(slave, 0, 0, CANOPEN_SDOABORT_INVALID_COMMAND);

    }
}

